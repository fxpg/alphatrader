#!/bin/sh

set -eu

HOST=$1
PORT=$2
IMAGE_TAG=$3
BUILD_TOKEN=$4
REGISTRY=$5

USERNAME=ec2-user
CONTAINER_NAME=grpc-server

ssh $HOST -l $USERNAME -p $PORT "
    docker login -u 'gitlab-ci-token' -p '$BUILD_TOKEN' $REGISTRY && \
    docker pull $IMAGE_TAG && \
    (docker stop $CONTAINER_NAME || true && docker rm $CONTAINER_NAME || true) && \
    docker run -d --rm --name $CONTAINER_NAME -p 50051:50051 $IMAGE_TAG grpc-server config.yml
"
echo "Deploy succeeded."
