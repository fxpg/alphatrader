#!/bin/bash

docker build -t arbitrager .
docker stop slack_bot | xargs docker rm -f
docker run -d --name slack_bot arbitrager slack_bot slack_bot/config.yml

