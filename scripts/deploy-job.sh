#!/bin/sh

set -eu

HOST=$1
PORT=$2
IMAGE_TAG=$3
BUILD_TOKEN=$4
REGISTRY=$5

USERNAME=ec2-user
CONTAINER_NAME=job-server

ssh $HOST -l $USERNAME -p $PORT "
    docker login -u 'gitlab-ci-token' -p '$BUILD_TOKEN' $REGISTRY && \
    docker pull $IMAGE_TAG && \
    (docker stop $CONTAINER_NAME || true && docker rm $CONTAINER_NAME || true) && \
    docker run -d --rm --name $CONTAINER_NAME $IMAGE_TAG job-server config.yml
"
echo "Deploy succeeded."
