package serviceapi

import (
	"gitlab.com/fxpg/alphatrader/models"
	"net/http"
	"net/http/httptest"
	"testing"
)

var (
	apiTestData       = make(map[string]string)
	apiTestDataMaster = map[string]string{
		"/public?command=returnTicker": `{"BTC_LTC":{"last":"0.0251","lowestAsk":"0.02589999","highestBid":"0.0251","percentChange":"0.02390438",
"baseVolume":"6.16485315","quoteVolume":"245.82513926"},"BTC_XRP":{"last":"0.00005730","lowestAsk":"0.00005710",
"highestBid":"0.00004903","percentChange":"0.16701570","baseVolume":"0.45347489","quoteVolume":"9094"}}`,
		"/public?command=returnOrderBook&currencyPair=BTC_XRP": `{"asks":[[0.00007600,1164],[0.00007620,1300]], "bids":[[0.00006901,200],[0.00006900,408]], "isFrozen": 0, "seq": 18849}`,
		"/public?command=returnCurrencies":                     `{"1CR":{"id":1,"name":"1CRedit","txFee":"0.01000000","minConf":3,"depositAddress":null,"disabled":0,"delisted":1,"frozen":0},"ABY":{"id":2,"name":"ArtByte","txFee":"0.01000000","minConf":8,"depositAddress":null,"disabled":0,"delisted":1,"frozen":0},"AC":{"id":3,"name":"AsiaCoin","txFee":"0.01000000","minConf":15,"depositAddress":null,"disabled":0,"delisted":1,"frozen":0},"ACH":{"id":4,"name":"Altcoin Herald","txFee":"0.00000000","minConf":5,"depositAddress":null,"disabled":0,"delisted":1,"frozen":0},"ADN":{"id":5,"name":"Aiden","txFee":"0.01000000","minConf":24,"depositAddress":null,"disabled":0,"delisted":1,"frozen":0}}`,
	}

	privApiTestData       = make(map[string]string)
	privApiTestDataMaster = map[string]string{
		"returnBalances": `{"1CR":"0.00000000","ABY":"0.00000000","AC":"0.00000000","ACH":"0.00000000","ADN":"0.00000000","AEON":"0.00000000","AERO":"0.00000000","AIR":"0.00000000","AMP":"0.00000000","APH":"0.00000000","ARCH":"0.00000000","ARDR":"0.00000000","AUR":"0.00000000","AXIS":"0.00000000","BALLS":"0.00000000","BANK":"0.00000000","BBL":"0.00000000","BBR":"0.00000000","BCC":"0.00000000","BCN":"0.00433735","BCY":"0.00000000","BDC":"0.00000000","BDG":"0.00000000","BELA":"0.00000000","BITCNY":"0.00000000","BITS":"0.00000000","BITUSD":"0.00000000","BLK":"0.00000000","BLOCK":"0.00000000","BLU":"0.00000000","BNS":"0.00000000","BONES":"0.00000000","BOST":"0.00000000","BTC":"0.00000001","BTCD":"0.00000000","BTCS":"0.00000000","BTM":"0.00000000","BTS":"0.24150001","BURN":"0.00000000","BURST":"0.00000000","C2":"0.00000000","CACH":"0.00000000","CAI":"0.00000000","CC":"0.00000000","CCN":"0.00000000","CGA":"0.00000000","CHA":"0.00000000","CINNI":"0.00000000","CLAM":"0.00000000","CNL":"0.00000000","CNMT":"0.00000000","CNOTE":"0.00000000","COMM":"0.00000000","CON":"0.00000000","CORG":"0.00000000","CRYPT":"0.00000000","CURE":"0.00000000","CYC":"0.00000000","DAO":"0.00000000","DASH":"0.00000000","DCR":"0.00000000","DGB":"0.00000000","DICE":"0.00000000","DIEM":"0.00000000","DIME":"0.00000000","DIS":"0.00000000","DNS":"0.00000000","DOGE":"0.00000000","DRKC":"0.00000000","DRM":"0.00000000","DSH":"0.00000000","DVK":"0.00000000","EAC":"0.00000000","EBT":"0.00000000","ECC":"0.00000000","EFL":"0.00000000","EMC2":"0.00000000","EMO":"0.00000000","ENC":"0.00000000","ETC":"0.00000000","ETH":"0.00000000","eTOK":"0.00000000","EXE":"0.00000000","EXP":"0.00000000","FAC":"0.00000000","FCN":"0.00000000","FCT":"0.00000000","FIBRE":"0.00000000","FLAP":"0.00000000","FLDC":"0.00000000","FLO":"0.00000000","FLT":"0.00000000","FOX":"0.00000000","FRAC":"0.00000000","FRK":"0.00000000","FRQ":"0.00000000","FVZ":"0.00000000","FZ":"0.00000000","FZN":"0.00000000","GAME":"0.00000000","GAP":"0.00000000","GDN":"0.00000000","GEMZ":"0.00000000","GEO":"0.00000000","GIAR":"0.00000000","GLB":"0.00000000","GML":"0.00000000","GNO":"0.00000000","GNS":"0.00000000","GNT":"0.00000000","GOLD":"0.00000000","GPC":"0.00000000","GPUC":"0.00000000","GRC":"0.00000000","GRCX":"0.00000000","GRS":"0.00000000","GUE":"0.00000000","H2O":"0.00000000","HIRO":"0.00000000","HOT":"0.00000000","HUC":"0.00000000","HUGE":"0.00000000","HVC":"0.00000000","HYP":"0.00000000","HZ":"0.00000000","IFC":"0.00000000","INDEX":"0.00000000","IOC":"0.00000000","ITC":"0.00000000","IXC":"0.00000000","JLH":"0.00000000","JPC":"0.00000000","JUG":"0.00000000","KDC":"0.00000000","KEY":"0.00000000","LBC":"0.00000000","LC":"0.00000000","LCL":"0.00000000","LEAF":"0.00000000","LGC":"0.00000000","LOL":"0.00000000","LOVE":"0.00000000","LQD":"0.00000000","LSK":"0.00000000","LTBC":"0.00000000","LTC":"0.00000000","LTCX":"0.00000000","MAID":"0.00000000","MAST":"0.00000000","MAX":"0.00000000","MCN":"0.00000000","MEC":"0.00000000","METH":"0.00000000","MIL":"0.00000000","MIN":"0.00000000","MINT":"0.00000000","MMC":"0.00000000","MMNXT":"0.00000000","MMXIV":"0.00000000","MNTA":"0.00000000","MON":"0.00000000","MRC":"0.00000000","MRS":"0.00000000","MTS":"0.00000000","MUN":"0.00000000","MYR":"0.00000000","MZC":"0.00000000","N5X":"0.00000000","NAS":"0.00000000","NAUT":"0.00000000","NAV":"0.00000000","NBT":"0.00000000","NEOS":"0.00000000","NL":"0.00000000","NMC":"0.00000000","NOBL":"0.00000000","NOTE":"0.00000000","NOXT":"0.00000000","NRS":"0.00000000","NSR":"0.00000000","NTX":"0.00000000","NXC":"0.00000000","NXT":"0.00000000","NXTI":"0.00000000","OMNI":"0.00000000","OPAL":"0.00000000","PAND":"0.00000000","PASC":"0.00000000","PAWN":"0.00000000","PIGGY":"0.00000000","PINK":"0.00000000","PLX":"0.00000000","PMC":"0.00000000","POT":"0.00000000","PPC":"0.00000000","PRC":"0.00000000","PRT":"0.00000000","PTS":"0.00000000","Q2C":"0.00000000","QBK":"0.00000000","QCN":"0.00000000","QORA":"0.00000000","QTL":"0.00000000","RADS":"0.00000000","RBY":"0.00000000","RDD":"0.00000000","REP":"0.00000000","RIC":"0.00000000","RZR":"0.00000000","SBD":"0.00000000","SC":"0.00000000","SDC":"0.00000000","SHIBE":"0.00000000","SHOPX":"0.00000000","SILK":"0.00000000","SJCX":"0.00000000","SLR":"0.00000000","SMC":"0.00000000","SOC":"0.00000000","SPA":"0.00000000","SQL":"0.00000000","SRCC":"0.00000000","SRG":"0.00000000","SSD":"0.00000000","STEEM":"0.00000000","STR":"0.00000000","STRAT":"0.00000000","SUM":"0.00000000","SUN":"0.00000000","SWARM":"0.00000000","SXC":"0.00000000","SYNC":"0.00000000","SYS":"0.00000000","TAC":"0.00000000","TOR":"0.00000000","TRUST":"0.00000000","TWE":"0.00000000","UIS":"0.00000000","ULTC":"0.00000000","UNITY":"0.00000000","URO":"0.00000000","USDE":"0.00000000","USDT":"0.00000000","UTC":"0.00000000","UTIL":"0.00000000","UVC":"0.00000000","VIA":"0.00000000","VOOT":"0.00000000","VOX":"0.00000000","VRC":"0.00000000","VTC":"0.00000000","WC":"0.00000000","WDC":"0.00000000","WIKI":"0.00000000","WOLF":"0.00000000","X13":"0.00000000","XAI":"0.00000000","XAP":"0.00000000","XBC":"0.00000000","XC":"0.00000000","XCH":"0.00000000","XCN":"0.00000000","XCP":"0.00000000","XCR":"0.00000000","XDN":"0.00000000","XDP":"0.00000000","XEM":"0.00000000","XHC":"0.00000000","XLB":"0.00000000","XMG":"0.00000000","XMR":"0.00000000","XPB":"0.00000000","XPM":"0.00000000","XRP":"0.00000000","XSI":"0.00000000","XST":"0.00000000","XSV":"0.00000000","XUSD":"0.00000000","XVC":"0.00000000","XXC":"0.00000000","YACC":"0.00000000","YANG":"0.00000000","YC":"0.00000000","YIN":"0.00000000","ZEC":"0.00000000"}`,
		"buy":            `{"orderNumber":"31226040","resultingTrades":[{"amount":"338.8732","date":"2014-10-18 23:03:21","rate":"0.00000173","total":"0.00058625","tradeID":"16164","type":"buy"}]}`,
		"returnDepositAddresses": `{"BTC":"19YqztHmspv2egyD6jQM3yn81x5t5krVdJ","LTC":"LPgf9kjv9H1Vuh4XSaKhzBe8JHdou1WgUB", "ITC":"Press Generate.."}`,
		"returnCompleteBalances": `{"LTC":{"available":"5.015","onOrders":"1.0025","btcValue":"0.078"}}`,
	}
)

func testHandleFunc() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var res string
		var ok bool
		if r.URL.String() == "/tradingApi" {
			cmd := r.FormValue("command")
			res, ok = privApiTestData[cmd]
			if !ok {
				res = "NODATA"
			}
		} else {
			res, ok = apiTestData[r.URL.String()]
			if !ok {
				res = "NODATA"
			}
		}

		w.Write([]byte(res))
	})
}

func testServer() *httptest.Server {
	for k, v := range apiTestDataMaster {
		apiTestData[k] = v
	}
	for k, v := range privApiTestDataMaster {
		privApiTestData[k] = v
	}
	return httptest.NewServer(testHandleFunc())
}

func TestPoloniexApiRate(t *testing.T) {
	s := testServer()
	defer s.Close()

	polo, err := NewPoloniexApiWithBaseURL("hoge", "fuga", s.URL, false)
	if err != nil {
		t.Fatalf("error: %s", err)
	}

	rate, err := polo.Rate(models.Ripple, models.Bitcoin)
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	if rate != 0.0000573 {
		t.Fatalf("got: %f", rate)
	}
}

func TestPoloniexApiBoard(t *testing.T) {
	s := testServer()
	defer s.Close()

	polo, err := NewPoloniexApiWithBaseURL("hoge", "fuga", s.URL, false)
	if err != nil {
		t.Fatalf("error: %s", err)
	}

	asks, bids, err := polo.Board(models.Bitcoin, models.Ripple)
	if err != nil {
		t.Fatalf("error: %s", err)
	}
	var p float64

	p = 0.0000760
	if asks[p] != 1164 {
		t.Fatalf("got: %f", asks[p])
	}
	p = 0.00007620
	if asks[p] != 1300 {
		t.Fatalf("got: %f", asks[p])
	}

	p = 0.00006901
	if bids[p] != 200 {
		t.Fatalf("got: %f", bids[p])
	}
	p = 0.000069
	if bids[p] != 408 {
		t.Fatalf("got: %f", bids[p])
	}
}

func TestPoloniexBalances(t *testing.T) {
	s := testServer()
	defer s.Close()

	polo, err := NewPoloniexApiWithBaseURL("", "", s.URL, false)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	bs, err := polo.Balances()
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	if bs[models.Bitcoin] != 0.00000001 {
		t.Fatalf("expected 0.00000001 but got %g", bs[models.Bitcoin])
	}
	if bs[models.Ripple] != 0 {
		t.Fatalf("expected 0 but got %g", bs[models.Bitcoin])
	}
	if bs[models.BitShares] != 0.24150001 {
		t.Fatalf("expected 0.24150001 but got %g", bs[models.BitShares])
	}
}

func TestPoloniexOrder(t *testing.T) {
	s := testServer()
	defer s.Close()

	polo, err := NewPoloniexApiWithBaseURL("", "", s.URL, false)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	_, err = polo.Order(models.Ripple, models.Bitcoin, models.Ask, 0.1234, 0.1111)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
}

func TestPoloniexAddress(t *testing.T) {
	s := testServer()
	defer s.Close()

	polo, err := NewPoloniexApiWithBaseURL("", "", s.URL, false)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	addr, err := polo.Address(models.Bitcoin)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	if addr != "19YqztHmspv2egyD6jQM3yn81x5t5krVdJ" {
		t.Fatalf("expected 19YqztHmspv2egyD6jQM3yn81x5t5krVdJ but got %s", addr)
	}
}

func TestPoloniexPrivateApiError(t *testing.T) {
	s := testServer()
	defer s.Close()

	privApiTestData["returnDepositAddresses"] = `{"error":"This is error"}`

	polo, err := NewPoloniexApiWithBaseURL("", "", s.URL, false)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	_, err = polo.Address(models.Bitcoin)
	if err == nil {
		t.Fatal("err == nil")
	}
}

func TestPoloniexCompleteBalances(t *testing.T) {
	s := testServer()
	defer s.Close()

	polo, err := NewPoloniexApiWithBaseURL("", "", s.URL, false)
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	b, err := polo.CompleteBalances()
	if err != nil {
		t.Fatalf("err: %s", err)
	}
	if ltc, ok := b[models.Litecoin]; !ok {
		t.Fatalf("ltf not found")
	} else if ltc.Available != 5.015 {
		t.Fatalf("expected 5.015 but got %g", ltc.Available)
	} else if ltc.OnOrders != 1.0025 {
		t.Fatalf("expected 1.0025 but got %g", ltc.Available)
	} else if ltc.Total() != 6.0175 {
		t.Fatalf("expected 6.0175 but got %g", ltc.Total())
	}
}

func TestPoloniexCurrencies(t *testing.T) {
	s := testServer()
	defer s.Close()

	polo, err := NewPoloniexApiWithBaseURL("", "", s.URL, false)
	if err != nil {
		t.Fatalf("err: %s", err)
	}

	c, err := polo.Currencies()
	if err != nil {
		t.Fatal(err)
	}

	if c["ABY"].Name != "ArtByte" {
		t.Fatal(c["ABY"])
	}
}
