package serviceapi

import (
	"gitlab.com/fxpg/alphatrader/models"
)

//interface定義
type ServiceApi interface {
	Rate(trading models.CurrencyType, settlement models.CurrencyType) (float64, error)
	//以下未実装
	Board(trading models.CurrencyType, settlement models.CurrencyType) (map[float64]float64, map[float64]float64, error)
	PurchaseFeeRate() (float64, error)
	SellFeeRate() (float64, error)
	TransferFee() (map[models.CurrencyType]float64, error)
	Balances() (map[models.CurrencyType]float64, error)
	CompleteBalances() (map[models.CurrencyType]*models.Balance, error)
	ActiveOrders() ([]models.Order, error)
	Order(trading models.CurrencyType, settlement models.CurrencyType,
		ordertype models.OrderType, price float64, amount float64) (int, error)
	Transfer(typ models.CurrencyType, addr string,
		amount float64, additionalFee float64) error
	CancelOrder(orderNumber int) error
	Address(c models.CurrencyType) (string, error)
}

type ExchangeApi interface {
	GetExchangeId() models.ExchangeID
	fetchRate() error
	Volume(trading string, settlement string) (float64, error)
	CurrencyPairs() ([]*CurrencyPair, error)
	Rate(trading string, settlement string) (float64, error)
}
