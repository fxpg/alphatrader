FROM golang:1.8-alpine3.6

# RUN apk add --update curl
# RUN curl https://glide.sh/get | sh
# RUN apk add --update git

RUN mkdir -p /go/src/gitlab.com/fxpg/alphatrader
WORKDIR /go/src/gitlab.com/fxpg/alphatrader

COPY ./ /go/src/gitlab.com/fxpg/alphatrader
# RUN glide install
RUN go install ./cmd/...
