package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

//go:generate enumer -type=TradeType
type TradeType int

const (
	Buy TradeType = iota + 1
	Sell

	UnknownSide
)

type OrderData struct {
	gorm.Model
	ExchangeID ExchangeID `json:"exchange_id"`
	TraderID   int        `json:"trader_id"`
	Pair       string     `json:"currency_pair"`
	OrderID    string     `json:"order_id"`

	Datetime  time.Time `json:"datetime"`
	Status    bool      `json:"status"`
	Price     float64   `json:"price"`
	Amount    float64   `json:"amount"`
	TradeType TradeType `json:"trade_type"`
}
