package models

import (
	"testing"
)

func TestRateMap(t *testing.T) {
	r := NewRateMap()

	r.Put(Bitcoin, Ripple, 1.234)
	r.Put(Burst, Ripple, 0.1233)

	var rate float64
	var ok bool

	rate, ok = r.Get(Bitcoin, Ripple)
	if !ok || rate != 1.234 {
		t.Fatalf("%t %f", ok, rate)
	}

	rate, ok = r.Get(Burst, Ripple)
	if !ok || rate != 0.1233 {
		t.Fatalf("%t %f", ok, rate)
	}

	rate, ok = r.Get(Bitcoin, Pinkcoin)
	if ok || rate != 0 {
		t.Fatalf("%t %f", ok, rate)
	}
}
