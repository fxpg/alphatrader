package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Name     string  `json:"name"`
	UserID   string  `gorm:"index" json:"user_id"`
	Password string  `json:"password"` // sha256 encoded
	Token    *string `json:"token"`
	Plan     string  `sql:"DEFAULT:'free'" json:"plan"`
}
