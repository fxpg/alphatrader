package models

import (
	"github.com/jinzhu/gorm"
)

type UserSetting struct {
	gorm.Model
	APIKey     string
	SecretKey  string
	ExchangeID ExchangeID `gorm:"index"` // iotaで定義している取引所ID
	UserID     int `gorm:"index"`
}
