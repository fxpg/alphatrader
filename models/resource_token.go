package models

import (
	"github.com/jinzhu/gorm"
)

type ResourceToken struct {
	gorm.Model
	UserID int
	Key    string
}
