package models

import (
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

// TODO: For the time being, Currency.Name is poloniex based.(abstract them later for multi market)
type Currency struct {
	Model
	Name     string `gorm:"unique_index" json:"name"` // ex: BTC
	FullName string `json:"full_name"`                // ex: Bitcoin
}

type CurrencyPairMap struct {
	Trading    string `json:"trading"`
	Settlement string `json:"settlement"`
}

type CurrencyRepository interface {
	Create(c *Currency) error
	FindAll() (map[string]Currency, error)
	DeleteByName(name string) error
	Truncate() error
}

func NewCurrencyDBRepository(db *gorm.DB) CurrencyRepository {
	return &currencyDBRepository{
		db: db,
	}
}

type currencyDBRepository struct {
	db *gorm.DB
}

func (r *currencyDBRepository) Create(c *Currency) error {
	if err := r.db.Create(c).Error; err != nil {
		return errors.Wrapf(err, "failed to save currency: %v", &c)
	}
	return nil
}

// returns map keyed by Currency.name
func (r *currencyDBRepository) FindAll() (map[string]Currency, error) {
	var cs []Currency
	if err := r.db.Find(&cs).Error; err != nil {
		return nil, errors.Wrap(err, "failed to find currency")
	}

	m := make(map[string]Currency)
	for _, c := range cs {
		m[c.Name] = c
	}
	return m, nil
}

func (r *currencyDBRepository) DeleteByName(name string) error {
	if err := r.db.Delete(&Currency{Name: name}).Error; err != nil {
		return errors.Wrapf(err, `failed to delete currency(name="%s")`, name)
	}
	return nil
}

func (r *currencyDBRepository) Truncate() error {
	return r.db.Exec("TRUNCATE TABLE currencies;").Error
}
