package models

import "github.com/jinzhu/gorm"

type TraderDistribution struct {
	gorm.Model
	TraderId int

	MasterIP          string
	WorkerContainerID string
	Status            string
}
