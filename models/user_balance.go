package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type UserBalance struct {
	gorm.Model

	UserID     uint       `gorm:"index"`
	ExchangeID ExchangeID `gorm:"index"`

	Currency CurrencyType `gorm:"index"`
	Balance  float64
	Datetime time.Time `gorm:"index"`
}
