package models

import (
	"github.com/jinzhu/gorm"
	"time"
)

type Chart struct {
	gorm.Model

	Last       float64
	Open       float64
	High       float64
	Low        float64
	Volume     float64
	Duration   int        `gorm:"index"` // (10,)60,3600,86400
	Datetime   time.Time  `gorm:"index"`
	Pair       string     `gorm:"index"`
	ExchangeID ExchangeID `gorm:"index"`
}

type ChartRepository interface {
	FindN(start time.Time, end time.Time, limit int) ([]Chart, error)
	Find(start time.Time, end time.Time) ([]Chart, error)
}

type chartStorage struct {
	db *gorm.DB

	duration     int
	exchangeID   ExchangeID
	currencyPair Cpair
}

func NewChartStorage(db *gorm.DB, duration int, exchangeID ExchangeID, currencyPair Cpair) ChartRepository {
	return &chartStorage{
		db:           db,
		duration:     duration,
		exchangeID:   exchangeID,
		currencyPair: currencyPair,
	}
}

/*
 * Retruns charts ordered by datetime descending.
 */
func (s *chartStorage) FindN(start time.Time, end time.Time, limit int) ([]Chart, error) {
	charts := make([]Chart, 0)

	q := s.db.Where("duration = ? and exchange_id = ? and pair = ?", s.duration,
		int(s.exchangeID), s.currencyPair.GetString())
	q = q.Where("? <= datetime and datetime <= ?", start, end)
	q = q.Order("datetime desc").Limit(limit)

	if err := q.Find(&charts).Error; err != nil {
		return nil, err
	}

	return charts, nil
}

func (s *chartStorage) Find(start time.Time, end time.Time) ([]Chart, error) {
	charts := make([]Chart, 0)

	q := s.db.Where("duration = ? and exchange_id = ? and pair = ?", s.duration,
		int(s.exchangeID), s.currencyPair.GetString())
	q = q.Where("? <= datetime and datetime <= ?", start, end)
	q = q.Order("datetime desc")

	if err := q.Find(&charts).Error; err != nil {
		return nil, err
	}

	return charts, nil
}
