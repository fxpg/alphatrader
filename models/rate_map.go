package models

import (
	"sync"
)

/*
 * 2通貨間のレートを保持するマップ
 */
type RateMap struct {
	m   map[CurrencyType]map[CurrencyType]float64
	mtx *sync.Mutex
}

func NewRateMap() *RateMap {
	return &RateMap{
		m:   make(map[CurrencyType]map[CurrencyType]float64),
		mtx: new(sync.Mutex),
	}
}

func (m *RateMap) Get(trading CurrencyType, settlement CurrencyType) (float64, bool) {
	m.mtx.Lock()
	defer m.mtx.Unlock()

	m2, ok := m.m[trading]
	if !ok {
		return 0, false
	}

	rate, ok := m2[settlement]
	if !ok {
		return 0, false
	}

	return rate, true
}

func (m *RateMap) Put(trading CurrencyType, settlement CurrencyType, rate float64) {
	m.mtx.Lock()
	defer m.mtx.Unlock()

	m2, ok := m.m[trading]
	if !ok {
		m2 = make(map[CurrencyType]float64)
		m.m[trading] = m2
	}

	m2[settlement] = rate
}
