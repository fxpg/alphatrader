package models

// 取引所
//go:generate enumer -type=ExchangeID
type ExchangeID int

const (
	Bitflyer ExchangeID = iota + 1
	Poloniex
	Coincheck
	Btcbox
	Yobit
	Hitbtc

	UnknownExchange
)

var ExchangeIDs = []ExchangeID{
	Bitflyer,
	Poloniex,
	Hitbtc,
}
