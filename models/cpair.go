package models

// 通貨ペア
type Cpair interface {
	GetString() string
	GetSettle() CurrencyType
	GetTrading() CurrencyType
}
type cpair struct {
	Trading CurrencyType
	Settle  CurrencyType
}

func (c *cpair) GetSettle() CurrencyType  { return c.Settle }
func (c *cpair) GetTrading() CurrencyType { return c.Trading }
func (c *cpair) GetString() string {
	str := CurrencyType2String(c.Settle) + "_" + CurrencyType2String(c.Trading)
	return str
}

func NewCurrencyPair(trading CurrencyType, settle CurrencyType) Cpair {
	return &cpair{
		Trading: trading,
		Settle:  settle,
	}

}
