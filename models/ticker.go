package models

import (
	"time"
)

type Ticker struct {
	Datetime   time.Time
	Rate       float64
	Volume     float64
	ExchangeID ExchangeID
	Pair       string
}
