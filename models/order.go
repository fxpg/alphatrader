package models

type OrderType int

const (
	Ask OrderType = iota
	Bid
)

type Order struct {
	ID         int
	Type       OrderType
	Trading    string
	Settlement string
	Price      float64
	Amount     float64
}
