package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/config"
	"os"
	"strings"
)

// TODO: 外だししたいが、Golandでテストをする際に、workdirが変わるのでfile.Openだとエラーになる
const configYaml = `
debug: true
test: true
db_connection: fxpg:sHD9xDkHPdTf@tcp(localhost:3306)/alphatrader_test?charset=utf8&parseTime=True&loc=UTC
`

func useCI() bool {
	env := os.Getenv("ALPHATRADER_USE_CI_ENV")
	if env == "" {
		return false
	}
	return true
}

func NewTestDB() *gorm.DB {
	var conf *config.Config

	if useCI() {
		conf = config.ReadConfig(os.Getenv("CWD") + "/config_test.yml")
	} else {
		conf = config.ReadConfigReader(strings.NewReader(configYaml))
	}

	db, err := gorm.Open("mysql", conf.DBConnection)
	db.LogMode(false)
	if err != nil {
		panic(errors.Wrap(err, "failed to open db"))
	}
	Migrate(db)

	return db
}
