package models

import (
	"time"
)

type Trader struct {
	ID              uint       `gorm:"primary_key" json:"id"`
	CreatedAt       time.Time  `json:"created_at"`
	UpdatedAt       time.Time  `json:"updated_at"`
	DeletedAt       *time.Time `sql:"index" json:"deleted_at"`
	UserID          int
	TickSec         int
	Description     string
	Code            string `gorm:"type:text"`
	Status          string
	TerminateReason string
}
