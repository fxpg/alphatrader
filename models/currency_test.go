package models

import (
	"github.com/jinzhu/gorm"
	"os"
	"testing"
)

var (
	db *gorm.DB
)

func TestMain(m *testing.M) {
	db = NewTestDB()
	db.AutoMigrate(&Currency{})
	os.Exit(m.Run())
}

func TestCurrencyDBRepository(t *testing.T) {
	repo := NewCurrencyDBRepository(db)
	if err := repo.Truncate(); err != nil {
		t.Fatal(err)
	}

	c1 := Currency{Name: "BTC", FullName: "Bitcoin"}
	c2 := Currency{Name: "DGB", FullName: "Digibyte"}

	if err := repo.Create(&c1); err != nil {
		t.Fatal(err)
	}
	if err := repo.Create(&c2); err != nil {
		t.Fatal(err)
	}
	if err := repo.Create(&c1); err == nil {
		t.Fatal("expected error")
	}

	cs, err := repo.FindAll()
	if err != nil {
		t.Fatal(err)
	}
	if len(cs) != 2 {
		t.Fatalf("len(cs) = %d", len(cs))
	}
	if cs["BTC"].FullName != "Bitcoin" {
		t.Fatalf(`cs["BTC"] = %v`, cs["BTC"])
	}
	if cs["DGB"].FullName != "Digibyte" {
		t.Fatalf(`cs["DGB"] = %v`, cs["DGB"])
	}
}

func TestCurrencyDBRepositoryDelete(t *testing.T) {
	repo := NewCurrencyDBRepository(db)
	if err := repo.Truncate(); err != nil {
		t.Fatal(err)
	}

	c1 := Currency{Name: "BTC", FullName: "Bitcoin"}
	if err := repo.Create(&c1); err != nil {
		t.Fatal(err)
	}

	if err := repo.DeleteByName("BTC"); err != nil {
		t.Fatal(err)
	}

	cs, err := repo.FindAll()
	if err != nil {
		t.Fatal(err)
	}
	if len(cs) != 0 {
		t.Fatalf("len(cs) = %d", len(cs))
	}
}
