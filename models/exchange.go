package models

type ExchangeMap struct {
	ID   ExchangeID `json:"id"`
	Name string     `json:"name"`
}
