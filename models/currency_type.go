package models

import (
	"github.com/pkg/errors"
	"strings"
)

//go:generate stringer -type=CurrencyType
type CurrencyType int

const (
	Ardor CurrencyType = iota + 1
	Augur
	Belacoin
	BitCrystals
	BitShares
	Bitcoin
	BitcoinDark
	BitcoinPlus
	Bitmark
	BlackCoin
	Burst
	Bytecoin
	CLAMS
	Counterparty
	DNotes
	Dash
	Decred
	DigiByte
	Dogecoin
	Einsteinium
	Ethereum
	EthereumClassic
	Expanse
	Factom
	Florincoin
	FoldingCoin
	GameCredits
	Gnosis
	Golem
	GridcoinResearch
	Huntercoin
	LBRYCredits
	Lisk
	Litecoin
	MaidSafeCoin
	Monero
	NAVCoin
	NEM
	NXT
	Namecoin
	Nautiluscoin
	Neoscoin
	Nexium
	Omni
	PascalCoin
	Peercoin
	Pinkcoin
	PotCoin
	Primecoin
	Radium
	Riecoin
	Ripple
	STEEM
	Siacoin
	SteemDollars
	Stellar
	StorjcoinX
	Stratis
	SynereoAMP
	Syscoin
	TetherUSD
	Vcash
	VeriCoin
	Vertcoin
	Viacoin
	Zcash

	Yen
	Unknown
)

func CurrencyType2String(c CurrencyType) string {
	switch c {
	case SynereoAMP:
		return "AMP"
	case Ardor:
		return "ARDR"
	case Bytecoin:
		return "BCN"
	case BitCrystals:
		return "BCY"
	case Belacoin:
		return "BELA"
	case BlackCoin:
		return "BLK"
	case Bitcoin:
		return "BTC"
	case BitcoinDark:
		return "BTCD"
	case Bitmark:
		return "BTM"
	case BitShares:
		return "BTS"
	case Burst:
		return "BURST"
	case CLAMS:
		return "CLAM"
	case Dash:
		return "DASH"
	case Decred:
		return "DCR"
	case DigiByte:
		return "DGB"
	case Dogecoin:
		return "DOGE"
	case Einsteinium:
		return "EMC2"
	case EthereumClassic:
		return "ETC"
	case Ethereum:
		return "ETH"
	case Expanse:
		return "EXP"
	case Factom:
		return "FCT"
	case FoldingCoin:
		return "FLDC"
	case Florincoin:
		return "FLO"
	case GameCredits:
		return "GAME"
	case Gnosis:
		return "GNO"
	case Golem:
		return "GNT"
	case GridcoinResearch:
		return "GRC"
	case Huntercoin:
		return "HUC"
	case LBRYCredits:
		return "LBC"
	case Lisk:
		return "LSK"
	case Litecoin:
		return "LTC"
	case MaidSafeCoin:
		return "MAID"
	case Nautiluscoin:
		return "NAUT"
	case NAVCoin:
		return "NAV"
	case Neoscoin:
		return "NEOS"
	case Namecoin:
		return "NMC"
	case DNotes:
		return "NOTE"
	case Nexium:
		return "NXC"
	case NXT:
		return "NXT"
	case Omni:
		return "OMNI"
	case PascalCoin:
		return "PASC"
	case Pinkcoin:
		return "PINK"
	case PotCoin:
		return "POT"
	case Peercoin:
		return "PPC"
	case Radium:
		return "RADS"
	case Augur:
		return "REP"
	case Riecoin:
		return "RIC"
	case SteemDollars:
		return "SBD"
	case Siacoin:
		return "SC"
	case StorjcoinX:
		return "SJCX"
	case STEEM:
		return "STEEM"
	case Stellar:
		return "STR"
	case Stratis:
		return "STRAT"
	case Syscoin:
		return "SYS"
	case TetherUSD:
		return "USDT"
	case Viacoin:
		return "VIA"
	case VeriCoin:
		return "VRC"
	case Vertcoin:
		return "VTC"
	case BitcoinPlus:
		return "XBC"
	case Counterparty:
		return "XCP"
	case NEM:
		return "XEM"
	case Monero:
		return "XMR"
	case Primecoin:
		return "XPM"
	case Ripple:
		return "XRP"
	case Vcash:
		return "XVC"
	case Zcash:
		return "ZEC"
	case Yen:
		return "Yen"
	default:
		return "UNKNOWN"
	}
}

func ParseCurrencyType(str string) (CurrencyType, error) {
	str = strings.ToUpper(str)
	switch str {
	case "AMP":
		return SynereoAMP, nil
	case "ARDR":
		return Ardor, nil
	case "BCN":
		return Bytecoin, nil
	case "BCY":
		return BitCrystals, nil
	case "BELA":
		return Belacoin, nil
	case "BLK":
		return BlackCoin, nil
	case "BTC":
		return Bitcoin, nil
	case "BTCD":
		return BitcoinDark, nil
	case "BTM":
		return Bitmark, nil
	case "BTS":
		return BitShares, nil
	case "BURST":
		return Burst, nil
	case "CLAM":
		return CLAMS, nil
	case "DASH":
		return Dash, nil
	case "DCR":
		return Decred, nil
	case "DGB":
		return DigiByte, nil
	case "DOGE":
		return Dogecoin, nil
	case "EMC2":
		return Einsteinium, nil
	case "ETC":
		return EthereumClassic, nil
	case "ETH":
		return Ethereum, nil
	case "EXP":
		return Expanse, nil
	case "FCT":
		return Factom, nil
	case "FLDC":
		return FoldingCoin, nil
	case "FLO":
		return Florincoin, nil
	case "GAME":
		return GameCredits, nil
	case "GNO":
		return Gnosis, nil
	case "GNT":
		return Golem, nil
	case "GRC":
		return GridcoinResearch, nil
	case "HUC":
		return Huntercoin, nil
	case "LBC":
		return LBRYCredits, nil
	case "LSK":
		return Lisk, nil
	case "LTC":
		return Litecoin, nil
	case "MAID":
		return MaidSafeCoin, nil
	case "NAUT":
		return Nautiluscoin, nil
	case "NAV":
		return NAVCoin, nil
	case "NEOS":
		return Neoscoin, nil
	case "NMC":
		return Namecoin, nil
	case "NOTE":
		return DNotes, nil
	case "NXC":
		return Nexium, nil
	case "NXT":
		return NXT, nil
	case "OMNI":
		return Omni, nil
	case "PASC":
		return PascalCoin, nil
	case "PINK":
		return Pinkcoin, nil
	case "POT":
		return PotCoin, nil
	case "PPC":
		return Peercoin, nil
	case "RADS":
		return Radium, nil
	case "REP":
		return Augur, nil
	case "RIC":
		return Riecoin, nil
	case "SBD":
		return SteemDollars, nil
	case "SC":
		return Siacoin, nil
	case "SJCX":
		return StorjcoinX, nil
	case "STEEM":
		return STEEM, nil
	case "STR":
		return Stellar, nil
	case "STRAT":
		return Stratis, nil
	case "SYS":
		return Syscoin, nil
	case "USDT":
		return TetherUSD, nil
	case "VIA":
		return Viacoin, nil
	case "VRC":
		return VeriCoin, nil
	case "VTC":
		return Vertcoin, nil
	case "XBC":
		return BitcoinPlus, nil
	case "XCP":
		return Counterparty, nil
	case "XEM":
		return NEM, nil
	case "XMR":
		return Monero, nil
	case "XPM":
		return Primecoin, nil
	case "XRP":
		return Ripple, nil
	case "XVC":
		return Vcash, nil
	case "ZEC":
		return Zcash, nil
	case "Yen":
		return Yen, nil
	default:
		return Unknown, errors.Errorf("unknown type %s", str)
	}
}
