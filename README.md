# arbitrager

自動アービトラージサーバー

## Requirements

- Go v1.7.3以上
- Godep
    + 導入方法: `$ go get github.com/tools/godep`
- Ta-lib
    + 導入方法 
    https://mrjbq7.github.io/ta-lib/install.html 参照

## Install

```
$ make install
```

## Run

```
$ make run
```
