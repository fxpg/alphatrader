package repositories

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/alphatrader/models"
)

type OrderRepositoryGorm struct {
	DB *gorm.DB `inject:""`
}

func (r *OrderRepositoryGorm) Create(order *models.OrderData) error {
	if err := r.DB.Create(order).Error; err != nil {
		return err
	}
	return nil
}

func (r *OrderRepositoryGorm) FindByTraderID(traderId int) ([]*models.OrderData, error) {
	orders := make([]*models.OrderData, 0)
	if err := r.DB.Where("trader_id = ?", traderId).Find(&orders).Error; err != nil {
		return orders, err
	}
	return orders, nil
}
