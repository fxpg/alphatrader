package repositories

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/alphatrader/models"
)

type UserSettingsRepositoryGorm struct {
	DB *gorm.DB `inject:""`
}

func (r *UserSettingsRepositoryGorm) Create(setting *models.UserSetting) error {
	if err := r.DB.Create(setting).Error; err != nil {
		return err
	}
	return nil
}

func (r *UserSettingsRepositoryGorm) FindByUserId(userId int) ([]*models.UserSetting, error) {
	var settings []*models.UserSetting
	if err := r.DB.Where("user_id = ?", userId).Find(&settings).Error; err != nil {
		return nil, err
	}
	return settings, nil
}
func (r *UserSettingsRepositoryGorm) FindById(settingId int) (*models.UserSetting, error) {
	var setting models.UserSetting
	if err := r.DB.Where("id = ?", settingId).Find(&setting).Error; err != nil {
		return nil, err
	}
	return &setting, nil
}

func (r *UserSettingsRepositoryGorm) IsExist(userId int, exchangeId models.ExchangeID) (ok bool, err error) {
	db := r.DB.Find(&models.UserSetting{}, "user_id = ? and exchange_id = ?", userId, exchangeId)

	if db.RecordNotFound() {
		return false, nil
	}
	if db.Error != nil {
		return false, db.Error
	}

	return true, nil
}

func (r *UserSettingsRepositoryGorm) Delete(setting *models.UserSetting) (bool, error) {
	db := r.DB.Unscoped().Delete(setting)
	if db.RecordNotFound() {
		return false, nil
	}
	if db.Error != nil {
		return false, db.Error
	}
	return true, nil
}
