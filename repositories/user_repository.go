package repositories

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/alphatrader/models"
)

type UserRepositoryGorm struct {
	DB *gorm.DB `inject:""`
}

func (r *UserRepositoryGorm) FindByStrId(userId string) (*models.User, error) {
	var user models.User

	db := r.DB.Where(&models.User{UserID: userId}).Find(&user)
	if db.RecordNotFound() {
		return nil, nil
	}
	if db.Error != nil {
		return nil, r.DB.Error
	}

	return &user, nil
}

func (r *UserRepositoryGorm) Create(user *models.User) error {
	if err := r.DB.Create(user).Error; err != nil {
		return err
	}
	return nil
}

func (r *UserRepositoryGorm) UpdateToken(user *models.User) error {
	if err := r.DB.Model(&user).Update("token", user.Token).Error; err != nil {
		return err
	}
	return nil
}

func (r *UserRepositoryGorm) UpdatePassword(user *models.User) error {
	if err := r.DB.Model(&user).Update("password", user.Password).Error; err != nil {
		return err
	}
	return nil
}
