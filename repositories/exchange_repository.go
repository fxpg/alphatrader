package repositories

import (
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/models"
	"gitlab.com/fxpg/alphatrader/serviceapi"
)

func generateExchangeMaps() []models.ExchangeMap {
	return []models.ExchangeMap{
		models.ExchangeMap{
			ID:   2,
			Name: "Poloniex",
		},
	}
}

func generateCUrrencyPairsFromExchangeAPI(exchangeIds []models.ExchangeID) map[models.ExchangeID][]models.CurrencyPairMap {
	m := make(map[models.ExchangeID][]models.CurrencyPairMap)
	for _, eid := range exchangeIds {
		eapi, err := serviceapi.NewExchangeAPI(eid)
		if err != nil {
			panic(err)
		}
		cpairs, err := eapi.CurrencyPairs()
		if err != nil {
			panic(err)
		}
		cPairMap := make([]models.CurrencyPairMap, 0)
		for _, cpair := range cpairs {
			cPairMap = append(cPairMap, models.CurrencyPairMap{
				Trading:    cpair.Trading,
				Settlement: cpair.Settlement,
			})
		}
		m[eid] = cPairMap
	}
	return m
}

func generateCurrencyPairs() map[models.ExchangeID][]models.CurrencyPairMap {
	m := make(map[models.ExchangeID][]models.CurrencyPairMap)

	// Poloniex
	cPairMap := make([]models.CurrencyPairMap, 0)
	tradingListForBitcoin := []models.CurrencyType{
		models.Ethereum, models.Stellar, models.Litecoin, models.Ripple,
		models.Vertcoin, models.Dash, models.Monero, models.Stratis, models.Zcash,
		models.EthereumClassic, models.Expanse, models.Dogecoin, models.NEM, models.Syscoin,
		models.BitShares, models.Viacoin, models.DigiByte, models.Factom, models.Gnosis,
		models.Siacoin, models.Lisk, models.NAVCoin, models.Decred, models.GameCredits,
		models.CLAMS, models.StorjcoinX, models.NXT, models.Counterparty, models.Nexium,
		models.Golem, models.GridcoinResearch, models.VeriCoin, models.Bytecoin,
		models.Ardor, models.Augur, models.Florincoin, models.Pinkcoin, models.Burst,
		models.Einsteinium, models.DNotes, models.BlackCoin, models.SynereoAMP,
		models.Neoscoin, models.BitCrystals, models.FoldingCoin, models.PotCoin,
		models.Riecoin, models.BitcoinPlus, models.Vcash, models.Primecoin,
		models.Namecoin, models.Radium, models.Bitmark, models.PascalCoin, models.BitcoinDark,
		models.Peercoin, models.Huntercoin, models.Nautiluscoin, models.SteemDollars,
	}
	for _, trading := range tradingListForBitcoin {
		cPairMap = append(cPairMap, models.CurrencyPairMap{
			Trading:    models.CurrencyType2String(trading),
			Settlement: models.CurrencyType2String(models.Bitcoin),
		})
	}
	tradingListForEthereum := []models.CurrencyType{
		models.Gnosis, models.Lisk, models.EthereumClassic, models.Zcash,
		models.Golem, models.STEEM, models.Augur,
	}
	for _, trading := range tradingListForEthereum {
		cPairMap = append(cPairMap, models.CurrencyPairMap{
			Trading:    models.CurrencyType2String(trading),
			Settlement: models.CurrencyType2String(models.Ethereum),
		})
	}
	tradingListForMonero := []models.CurrencyType{
		models.Litecoin, models.Zcash, models.NXT, models.Dash,
		models.BitcoinDark, models.MaidSafeCoin, models.BlackCoin,
	}
	for _, trading := range tradingListForMonero {
		cPairMap = append(cPairMap, models.CurrencyPairMap{
			Trading:    models.CurrencyType2String(trading),
			Settlement: models.CurrencyType2String(models.Ethereum),
		})
	}
	tradingListForTetherUSD := []models.CurrencyType{
		models.Bitcoin, models.Ethereum, models.Stellar, models.Litecoin,
		models.Zcash, models.EthereumClassic, models.Dash, models.Monero,
		models.NXT, models.Augur,
	}
	for _, trading := range tradingListForTetherUSD {
		cPairMap = append(cPairMap, models.CurrencyPairMap{
			Trading:    models.CurrencyType2String(trading),
			Settlement: models.CurrencyType2String(models.TetherUSD),
		})
	}
	m[2] = cPairMap

	return m
}

type exchangeRepositoryOnMemory struct {
	exchangeMaps  []models.ExchangeMap
	currencyPairs map[models.ExchangeID][]models.CurrencyPairMap
}

func NewExchangeStorageOnMemory() *exchangeRepositoryOnMemory {
	return &exchangeRepositoryOnMemory{

		generateExchangeMaps(),
		generateCurrencyPairs(),
	}
}

func (e *exchangeRepositoryOnMemory) FindAll() ([]models.ExchangeMap, error) {
	return e.exchangeMaps, nil
}

func (e *exchangeRepositoryOnMemory) FindCurrencyPairsByExchangeId(exchangeId models.ExchangeID) ([]models.CurrencyPairMap, error) {
	currencyPairs, ok := e.currencyPairs[exchangeId]
	if !ok {
		return currencyPairs, errors.New("failed to get currency pairs")
	}
	return currencyPairs, nil
}

type exchangeRepositoryFromExchangeAPI struct {
	exchangeMaps  []models.ExchangeMap
	currencyPairs map[models.ExchangeID][]models.CurrencyPairMap
}

func NewExchangeRepositoryFromExchangeAPI(exchangeIds []models.ExchangeID) *exchangeRepositoryFromExchangeAPI {
	exchangeMap := make([]models.ExchangeMap, 0)
	for _, exchangeId := range exchangeIds {
		exchangeMap = append(exchangeMap, models.ExchangeMap{ID: exchangeId, Name: exchangeId.String()})

	}
	return &exchangeRepositoryFromExchangeAPI{
		exchangeMap,
		generateCUrrencyPairsFromExchangeAPI(exchangeIds),
	}
}

func (e *exchangeRepositoryFromExchangeAPI) FindAll() ([]models.ExchangeMap, error) {
	return e.exchangeMaps, nil
}

func (e *exchangeRepositoryFromExchangeAPI) FindCurrencyPairsByExchangeId(exchangeId models.ExchangeID) ([]models.CurrencyPairMap, error) {
	currencyPairs, ok := e.currencyPairs[exchangeId]
	if !ok {
		return currencyPairs, errors.New("failed to get currency pairs")
	}
	return currencyPairs, nil
}
