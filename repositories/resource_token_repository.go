package repositories

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/alphatrader/models"
)

type ResourceTokenRepositoryGorm struct {
	DB *gorm.DB `inject:""`
}

func (r *ResourceTokenRepositoryGorm) Create(token *models.ResourceToken) error {
	if err := r.DB.Create(token).Error; err != nil {
		return err
	}
	return nil
}

func (r *ResourceTokenRepositoryGorm) IsExist(userId int) (bool, error) {
	var exists bool
	db := r.DB.Raw(`
SELECT EXISTS (
  SELECT * FROM resource_token WHERE (user_id == ?)
) `, userId).Scan(&exists)
	if db.Error != nil {
		return false, db.Error
	}

	return exists, nil
}

func (r *ResourceTokenRepositoryGorm) FindByUserId(userId int) (*models.ResourceToken, error) {
	var token models.ResourceToken
	db := r.DB.Where("user_id = ?", userId).Find(&token)
	if db.RecordNotFound() {
		return nil, nil
	}
	if db.Error != nil {
		return nil, r.DB.Error
	}
	if err := r.DB.Error; err != nil {
		return &token, err
	}
	return &token, nil
}

func (r *ResourceTokenRepositoryGorm) FindByKey(key string) (*models.ResourceToken, error) {
	var token models.ResourceToken

	db := r.DB.Where(&models.ResourceToken{Key: key}).Find(&token)
	if db.RecordNotFound() {
		return nil, nil
	}
	if db.Error != nil {
		return nil, r.DB.Error
	}

	return &token, nil
}
