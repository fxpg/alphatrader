package repositories

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/alphatrader/models"
)

type TraderRepositoryGorm struct {
	DB *gorm.DB `inject:""`
}

func (r *TraderRepositoryGorm) Create(trader *models.Trader) (traderId int, err error) {
	if err := r.DB.Create(trader).Error; err != nil {
		return int(trader.ID), err
	}
	return int(trader.ID), nil
}

func (r *TraderRepositoryGorm) FindByUserId(userId int) ([]*models.Trader, error) {
	traders := make([]*models.Trader, 0)
	if err := r.DB.Where("user_id = ?", userId).Find(&traders).Error; err != nil {
		return traders, err
	}
	return traders, nil
}

func (r *TraderRepositoryGorm) FindById(traderId int) (*models.Trader, error) {
	var trader models.Trader

	if err := r.DB.Where("id = ?", traderId).Find(&trader).Error; err != nil {
		return &trader, err
	}

	return &trader, nil
}

func (r *TraderRepositoryGorm) UpdateStatus(traderId int, status string) (err error) {
	if err := r.DB.Model(&models.Trader{
		ID: uint(traderId),
	}).Update("status", status).Error; err != nil {
		return err
	}
	return nil
}
func (r *TraderRepositoryGorm) Terminate(traderId int, reason string) (err error) {
	if err := r.DB.Model(&models.Trader{
		ID: uint(traderId),
	}).Update("status", "terminated").Update("reason", reason).Error; err != nil {
		return err
	}
	return nil
}
func (r *TraderRepositoryGorm) FindByStatus(status string) ([]*models.Trader, error) {
	var traders []*models.Trader

	if err := r.DB.Where("status = ?", status).Find(&traders).Error; err != nil {
		return traders, err
	}

	return traders, nil
}
func (r *TraderRepositoryGorm) FindRunningTrader(duration int) ([]*models.Trader, error) {
	var traders []*models.Trader
	if err := r.DB.Where("tick_sec = ? and status = ?", duration, "running").Find(&traders).Error; err != nil {
		return traders, err
	}

	return traders, nil
}

func (r *TraderRepositoryGorm) Delete(traderId int) (bool, error) {
	var trader models.Trader
	trader.ID = uint(traderId)

	db := r.DB.Delete(trader)
	if db.RecordNotFound() {
		return false, nil
	}
	if db.Error != nil {
		return false, db.Error
	}
	return true, nil
}

func (r *TraderRepositoryGorm) ChangeStoppingToStopped() error {
	if err := r.DB.Table("traders").Where("status = ?", "stopping").Update("status", "stopped").Error; err != nil {
		return err
	}
	return nil
}
