package repositories

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elbv2"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/config"
)

type GrpcServerRepositoryLocal struct {
	Config *config.Config `inject:""`
}

func (r *GrpcServerRepositoryLocal) GetLoadBalancers() ([]string, error) {
	return []string{"localhost"}, nil
}

type GrpcServerRepositoryEC2 struct {
	Config *config.Config `inject:""`
}

func (r *GrpcServerRepositoryEC2) GetLoadBalancers() ([]string, error) {
	fmt.Println(r.Config.EC2.AWS_ACCESS_KEY_ID)
	fmt.Println(r.Config.EC2.AWS_SECRET_ACCESS_KEY)

	creds := credentials.NewStaticCredentials(r.Config.EC2.AWS_ACCESS_KEY_ID, r.Config.EC2.AWS_SECRET_ACCESS_KEY, "")
	sess, err := session.NewSession(&aws.Config{
		Region:      &r.Config.EC2.AWS_REGION,
		Credentials: creds,
	})
	if err != nil {
		return []string{}, err
	}
	svc := elbv2.New(sess)
	input := &elbv2.DescribeLoadBalancersInput{
		Names: []*string{
			aws.String("atmaster"),
		},
	}
	result, err := svc.DescribeLoadBalancers(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			return []string{}, errors.New(aerr.Error())

		}
		return []string{}, err
	}

	if len(result.LoadBalancers) == 0 {
		return []string{}, errors.New("there is no available atmaster loadbalancer")
	}
	ips := make([]string, 0)
	for _, addr := range result.LoadBalancers {
		ips = append(ips, *addr.DNSName)
	}
	return ips, nil
}
