package repositories

import (
	"testing"
	"gitlab.com/fxpg/alphatrader/models"
	"github.com/stretchr/testify/assert"
)

func TestUserSettingsRepositoryGorm_Basic(t *testing.T) {
	db := models.NewTestDB()
	tx := db.Begin()
	defer tx.Rollback()

	r := UserSettingsRepositoryGorm{tx}

	setting := models.UserSetting{
		UserID: 123,
		ExchangeID: models.Poloniex,
		APIKey: "user1_apikey",
		SecretKey: "user1_secretkey",
	}
	if err := r.Create(&setting); err != nil {
		assert.NoError(t, err)
	}

	settings, err := r.FindByUserId("123")
	if err != nil {
		assert.NoError(t, err)
	}
	assert.Equal(t, 1, len(settings))
	assert.Equal(t, 123, settings[0].UserID)
	assert.Equal(t, models.Poloniex, settings[0].ExchangeID)
	assert.Equal(t, "user1_apikey", settings[0].APIKey)
}