BINNAME = arbitrager
GO = go
PKGS = `glide nv`

submodule:
	git submodule foreach git pull origin master

glide:
	glide update ./...

test:
	export CWD=${PWD} && $(GO) test -cover $(PKGS)

test-verbose:
	export CWD=${PWD} && $(GO) test -cover $(PKGS)

test-short:
	export CWD=${PWD} && $(GO) test -cover $(PKGS) -short

build:
	$(GO) build $(PKGS)

docker-build:
	docker build . -t alphatrader

run-front-debug:install-front-server
	front-server config_local.yml

run-chart-docker-debug:
	docker run -it --net=internet --rm --name chart-server2 alphatrader  chart-server2 config_test.yml

run-front-docker-debug:
	docker run -it --net=internet --rm --name alphatrader -p 8080:8080 alphatrader  front-server config_test.yml

run-grpc-docker-debug:
	docker run -it --net=internet --rm --name grpc-server alphatrader  grpc-server config_test.yml

run-job-docker-debug:
	docker run -it --net=internet --rm --name job-server alphatrader job-server config_test.yml

run-mysql-server:
	(docker stop mysql || true) && (docker rm mysql || true)
	docker run --volumes-from mysql_data --name mysql -p 3307:3306 -e MYSQL_ROOT_PASSWORD=mysql -d --net=internet --hostname mysql mysql --character-set-server=utf8 --collation-server=utf8_unicode_ci

create-mysql-container:
	docker run -v /var/lib/mysql --name mysql_data busybox


front-server-debug:install-front-server
	cd cmd/executor && go run *.go config_local.yml

front-server:install-front-server
	front-server cmd/front-server/config.yml

install-front-server:
	cd cmd/front-server && go install $(glide nv)

chart-server:install-chart-server
	chart-server cmd/chart-server/config.yml

install-chart-server:
	cd cmd/chart-server && go install $(glide nv)

install:
	$(GO) install $(PKGS)

grpc-server:install-grpc-server
	grpc-server cmd/grpc-server/config.yml

grpc-server-debug:
	cd cmd/grpc-server && go run *.go config_local.yml

install-grpc-server:
	cd cmd/grpc-server && go install &(glide nv)

.PHONY: glide test test-verbose build run-server bindata executor-debug executor install-executor chart-server install-chart-server grpc-server grpc-server-debug install-grpc-server
