package main

import (
	"encoding/json"
	"fmt"
	"github.com/gocarina/gocsv"
	"github.com/pkg/errors"
	"log"
	"net/http"
	"sort"
	"strconv"
	"time"
)

const (
	urlBase = "https://api.cryptowat.ch"
)

type OHLC struct {
	Timestamp int64
	Open      float64
	High      float64
	Low       float64
	Close     float64
	Volume    float64
}

type OHLCSlice []OHLC

func (s OHLCSlice) Len() int {
	return len(s)
}

func (s OHLCSlice) Less(i, j int) bool {
	return s[i].Timestamp < s[j].Timestamp
}

func (s OHLCSlice) Swap(i, j int) {
	tmp := s[i]
	s[i] = s[j]
	s[j] = tmp
}

type cwOHLCResponse struct {
	Result map[string][][]float64 `json:"result"`
}

func fetchOHLC(from time.Time, to time.Time, period int) ([]OHLC, error) {
	url := fmt.Sprintf("%s/markets/poloniex/xrpbtc/ohlc?before=%d&after=%d&periods=%d", urlBase,
		to.Unix(), from.Unix(), period)
	log.Print(url)

	res, err := http.Get(url)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch url")
	}
	defer res.Body.Close()

	var ohlcRes cwOHLCResponse
	if err := json.NewDecoder(res.Body).Decode(&ohlcRes); err != nil {
		return nil, errors.Wrap(err, "failed to parse response body")
	}

	ohlcs := make([]OHLC, 0, len(ohlcRes.Result[strconv.FormatInt(int64(period), 10)]))
	for _, x := range ohlcRes.Result[strconv.FormatInt(int64(period), 10)] {
		ohlc := OHLC{
			Timestamp: int64(x[0]),
			Open:      x[1],
			High:      x[2],
			Low:       x[3],
			Close:     x[4],
			Volume:    x[5],
		}
		ohlcs = append(ohlcs, ohlc)
	}

	sort.Sort(sort.Reverse(OHLCSlice(ohlcs)))

	return ohlcs, nil
}

func main() {
	to := time.Now().AddDate(-1, 0, -14)
	from := to.Add(-24 * time.Hour)
	ohlc, err := fetchOHLC(from, to, 300)
	if err != nil {
		log.Fatal(err)
	}
	str, err := gocsv.MarshalString(ohlc)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(str)
}
