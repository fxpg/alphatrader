package types

import ()

//go:generate mockery -name=GrpcService
type GrpcService interface {
	Auth(userId int, resourceToken string) (bool, error)
	GenerateResourceToken(userId int) (string, error)
}
