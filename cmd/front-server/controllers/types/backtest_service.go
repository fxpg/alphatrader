package types

import (
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/models"
	"time"
)

var (
	BacktestValidationError = errors.New("validation error")
)

type BacktestSetting struct {
	UserID         int
	ExchangeId     models.ExchangeID
	From           time.Time
	To             time.Time
	LossCutRate    float64
	ProfitTakeRate float64
	Trading        models.CurrencyType
	Settlement     models.CurrencyType
	TickSec        int
}

type BacktestResult struct {
	Status       string
	ResultString string
	Setting      BacktestSetting
}

//go:generate mockery -name=BacktestsService
type BacktestsService interface {
	// バックテストを実行し、backtestIdを返す
	// MEMO: キューにして非同期にする場合も、とりあえずユニークなIDを生成して返す
	// Yamlのバリデーションエラー等、入力値に問題があった場合は、BacktestValidationErrorをWrapして返す
	// TODO: ユーザーごとの最大起動数も仕様定義が必要
	Run(userId string, setting *BacktestSetting) (int, error)
	GetConfig(backtestId int) (*BacktestSetting, error)
	GetResult(backtestId int) (*BacktestResult, error)
}
