package types

import (
	"gitlab.com/fxpg/alphatrader/models"
)

//go:generate mockery -name=ExchangesService
type ExchangesService interface {
	GetCurrencyPairsByExchangeID(exchangeId int) []models.CurrencyPairMap
	GetExchanges() []models.ExchangeMap
}
