package types

import "gitlab.com/fxpg/alphatrader/models"

//go:generate mockery -name=SettingsService
type SettingsService interface {
	Create(userId int, exchangeId models.ExchangeID,
		apiKey string, secretKey string) error

	FindAll(userId int) ([]*models.UserSetting, error)
	IsExist(userId int, exchangeId models.ExchangeID) (bool bool, err error)
	DeleteUserSetting(userId int, settingId int) (bool, error)
}
