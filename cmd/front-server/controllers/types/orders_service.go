package types

import (
	"gitlab.com/fxpg/alphatrader/models"
	"time"
)

//go:generate mockery -name=OrdersService
type OrdersService interface {
	Insert(userId int, traderId int, datetime time.Time, exchange string, pair string, orderId string, amount float64, price float64, order_type models.TradeType) error
	FindByTraderId(traderId int) ([]*models.OrderData, error)
}
