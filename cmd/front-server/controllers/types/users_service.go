package types

import (
	"gitlab.com/fxpg/alphatrader/models"
)

//go:generate mockery -name=UsersService
type UsersService interface {
	IsExists(userId string) (bool, error)
	Create(userId string, password string) error
	Authenticate(userId string, token string) (bool, error)
	AuthenticateWithPassword(userId string, password string) (bool, error)
	FindByUserId(userId string) (*models.User, error)
	FindUsersExchange(userId int) ([]models.ExchangeID, error)
	UpdateToken(userId string) (string, error)
	UpdatePassword(userId string, newPassword string) error
}
