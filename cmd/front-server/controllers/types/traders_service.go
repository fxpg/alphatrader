package types

import (
	"time"

	"gitlab.com/fxpg/alphatrader/models"
)

type OrderType int

const (
	BUY OrderType = iota
	SELL
)

func (ot OrderType) String() string {
	switch ot {
	case BUY:
		return "BUY"
	case SELL:
		return "SELL"
	default:
		return "Unknown"
	}
}

type TraderSetting struct {
	Description string
	UserId      int
	TickSec     int
	Code        string
	Token       string
}

type TraderStatus struct {
	TraderSetting
	Status string
}
type TraderResult struct {
	Status string
	Code   string
}

type Order struct {
	UserId    int
	Datetime  time.Time
	Exchange  string
	Pair      string
	Amount    float64
	Price     float64
	OrderType OrderType
}

//go:generate mockery -name=TradersService
type TradersService interface {
	// トレーダーを実行し、traderIdを返す
	// MEMO: キューにして非同期にする場合も、とりあえずユニークなIDを生成して返す
	// TODO: ユーザーごとの最大起動数も仕様定義が必要
	Set(trader *TraderSetting) (int, error)
	Run(traderId int) error
	GetTrader(traderId int) (*models.Trader, error)
	GetTraders(userId int) ([]*models.Trader, error)
	ChangeStatus(traderId int, status string) error
	Delete(traderId int) error
	Terminate(traderId int, reason string) error
	GetTradersByDuration(duration int) ([]*models.Trader, error)
	ChangeStoppingToStopped() error
}
