// Code generated by mockery v1.0.0
package mocks

import mock "github.com/stretchr/testify/mock"
import models "gitlab.com/fxpg/alphatrader/models"

// ExchangesService is an autogenerated mock type for the ExchangesService type
type ExchangesService struct {
	mock.Mock
}

// GetCurrencyPairsByExchangeID provides a mock function with given fields: exchangeId
func (_m *ExchangesService) GetCurrencyPairsByExchangeID(exchangeId int) []models.CurrencyPairMap {
	ret := _m.Called(exchangeId)

	var r0 []models.CurrencyPairMap
	if rf, ok := ret.Get(0).(func(int) []models.CurrencyPairMap); ok {
		r0 = rf(exchangeId)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.CurrencyPairMap)
		}
	}

	return r0
}

// GetExchanges provides a mock function with given fields:
func (_m *ExchangesService) GetExchanges() []models.ExchangeMap {
	ret := _m.Called()

	var r0 []models.ExchangeMap
	if rf, ok := ret.Get(0).(func() []models.ExchangeMap); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]models.ExchangeMap)
		}
	}

	return r0
}
