package controllers

import (
	"fmt"

	"github.com/labstack/echo"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
)

const (
	HEADER_KEY_USERID = "X-ALP-UserID"
	HEADER_KEY_TOKEN  = "X-ALP-Token"
)

func AuthV2(s types.UsersService) echo.MiddlewareFunc {
	return func(f echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			userId := c.Request().Header.Get(HEADER_KEY_USERID)
			token := c.Request().Header.Get(HEADER_KEY_TOKEN)
			if ok, err := s.Authenticate(userId, token); err != nil {
				fmt.Println(err)
				return err
			} else if !ok {
				return Error(c, ER107)
			}
			return f(c)
		}
	}
}
