package controllers

import (
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types/mocks"
	"gitlab.com/fxpg/alphatrader/models"
	"strings"
)

func TestGetCurrencyPairs(t *testing.T) {
	e := echo.New()

	exchangesService := new(mocks.ExchangesService)
	exchangesService.On("GetCurrencyPairsByExchangeID", mock.Anything).Return([]models.CurrencyPairMap{
		{Trading: "XRP", Settlement: "BTC"},
		{Trading: "XEM", Settlement: "BTC"},
	})

	controller := &ExchangesController{
		ExchangesService: exchangesService,
	}

	req := httptest.NewRequest(echo.GET, "/exchanges/1/currency_pairs", strings.NewReader(``))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)
	c.SetPath("/exchanges/1/currency_pairs")

	if assert.NoError(t, controller.getCurrencyPairs(c)) {
		assert.Equal(t, `[{"trading":"XRP","settlement":"BTC"},{"trading":"XEM","settlement":"BTC"}]`, rec.Body.String())
		exchangesService.AssertCalled(t, "GetCurrencyPairsByExchangeID", 1)
	}
}

func TestGetExchanges(t *testing.T) {
	e := echo.New()
	group := e.Group("")

	exchangesService := new(mocks.ExchangesService)
	exchangesService.On("GetExchanges").Return([]models.ExchangeMap{
		{ID: 1, Name: "Poloniex"},
		{ID: 2, Name: "Bitflyer"},
	})

	controller := &ExchangesController{
		ExchangesService: exchangesService,
	}
	controller.Register(group)

	req := httptest.NewRequest(echo.GET, "/exchanges", strings.NewReader(``))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.getExchanges(c)) {
		assert.Equal(t, `[{"id":1,"name":"Poloniex"},{"id":2,"name":"Bitflyer"}]`, rec.Body.String())
		exchangesService.AssertCalled(t, "GetExchanges")
	}
}
