package controllers

import (
	"net/http"
	"regexp"

	"github.com/labstack/echo"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
	"gitlab.com/fxpg/alphatrader/models"
)

type UsersController struct {
	UsersService   types.UsersService   `inject:""`
	TradersService types.TradersService `inject:""`
}

func (u *UsersController) Register(g *echo.Group) {
	g.POST("/users", u.createUser)
	g.GET("/users", u.getUser, AuthV2(u.UsersService))
	g.GET("/users/resource", u.getUserResource, AuthV2(u.UsersService))
	g.POST("/users/_login", u.loginUser)
	g.PATCH("/users/password", u.updatePassword, AuthV2(u.UsersService))
}

func (u *UsersController) createUser(c echo.Context) error {
	var in struct {
		UserID   string `json:"user_id"`
		Password string `json:"password"`
	}
	if err := c.Bind(&in); err != nil {
		return Error(c, ER001)
	}

	if in.UserID == "" {
		return Error(c, ER102)
	}
	if in.Password == "" {
		return Error(c, ER103)
	}

	if ok, err := u.UsersService.IsExists(in.UserID); err != nil {
		return err
	} else if ok {
		return Error(c, ER101)
	}

	r := "[^a-zA-Z0-9]"
	r2 := "[^a-zA-Z0-9!-.]"
	if regexp.MustCompile(r).Match([]byte(in.UserID)) {
		return Error(c, ER104)
	}
	if len(in.UserID) < 5 || len(in.UserID) > 16 {
		return Error(c, ER104)
	}
	if regexp.MustCompile(r2).Match([]byte(in.Password)) {
		return Error(c, ER105)
	}
	if len(in.Password) < 8 {
		return Error(c, ER105)
	}

	ab := "[a-zA-Z]"
	n := "[0-9]"
	sym := "[!-.]"
	if !regexp.MustCompile(ab).Match([]byte(in.Password)) {
		return Error(c, ER105)
	}
	if !regexp.MustCompile(n).Match([]byte(in.Password)) {
		return Error(c, ER105)
	}
	if !regexp.MustCompile(sym).Match([]byte(in.Password)) {
		return Error(c, ER105)
	}
	if err := u.UsersService.Create(in.UserID, in.Password); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		Status string `json:"status"`
	}{"ok"})
}

func (u *UsersController) getUserResource(c echo.Context) error {
	userId := c.Request().Header.Get(HEADER_KEY_USERID)

	user, err := u.UsersService.FindByUserId(userId)
	if err != nil {
		return err
	}
	if user == nil {
		return Error(c, ER002)
	}
	var nOfTrader int
	switch user.Plan {
	case "free":
		nOfTrader = 2
	case "pro":
		nOfTrader = 5
	default:
		nOfTrader = 3
	}
	traders, err := u.TradersService.GetTraders(int(user.ID))
	if err != nil {
		return err
	}
	return c.JSON(http.StatusOK, struct {
		Plan        string `json:"plan"`
		TraderMax   int    `json:"trader_max"`
		TraderSaved int    `json:"trader_saved"`
	}{
		Plan:        user.Plan,
		TraderMax:   nOfTrader,
		TraderSaved: len(traders),
	})
}

func (u *UsersController) getUser(c echo.Context) error {
	userId := c.Request().Header.Get(HEADER_KEY_USERID)

	user, err := u.UsersService.FindByUserId(userId)
	if err != nil {
		return err
	}
	if user == nil {
		return Error(c, ER002)
	}

	exchanges, err := u.UsersService.FindUsersExchange(int(user.ID))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		Name      string              `json:"name"`
		Exchanges []models.ExchangeID `json:"exchanges"`
	}{
		Name:      user.Name,
		Exchanges: exchanges,
	})
}

func (u *UsersController) loginUser(c echo.Context) error {
	var in struct {
		UserID   string `json:"user_id"`
		Password string `json:"password"`
	}
	if err := c.Bind(&in); err != nil {
		return Error(c, ER001)
	}

	if ok, err := u.UsersService.AuthenticateWithPassword(in.UserID, in.Password); err != nil {
		return err
	} else if !ok {
		return Error(c, ER107)
	}

	token, err := u.UsersService.UpdateToken(in.UserID)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		Token string `json:"token"`
	}{
		Token: token,
	})
}

func (u *UsersController) updatePassword(c echo.Context) error {
	userId := c.Request().Header.Get(HEADER_KEY_USERID)

	var in struct {
		NewPassword  string `json:"new_password"`
		NewPassword2 string `json:"new_password2"`
	}
	if err := c.Bind(&in); err != nil {
		return Error(c, ER001)
	}
	if in.NewPassword != in.NewPassword2 {
		return Error(c, ER106)
	}
	if err := u.UsersService.UpdatePassword(userId, in.NewPassword); err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		Status string `json:"status"`
	}{
		Status: "ok",
	})
}
