package controllers

import (
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types/mocks"

	"github.com/stretchr/testify/mock"
	"strings"
)

func TestUserCreate(t *testing.T) {
	e := echo.New()

	usersService := new(mocks.UsersService)
	usersService.On("IsExists", mock.Anything).
		Return(false, nil)
	usersService.On("Create", "user_id", "password").
		Return(nil)

	controller := &UsersController{
		UsersService: usersService,
	}

	req := httptest.NewRequest(echo.POST, "/users",
		strings.NewReader(`{"user_id": "user_id", "password": "password"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.createUser(c)) {
		assert.Equal(t, `{"status":"ok"}`, rec.Body.String())
	}
}

func TestUpdatePassword(t *testing.T) {
	e := echo.New()

	usersService := new(mocks.UsersService)
	usersService.On("UpdatePassword", mock.Anything, mock.Anything).Return(nil)
	usersService.On("IsExists", mock.Anything).Return(false, nil)

	controller := &UsersController{
		UsersService: usersService,
	}

	req := httptest.NewRequest(echo.POST, "/users",
		strings.NewReader(`{"new_password": "fuga"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	req.Header.Set(HEADER_KEY_USERID, "test")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.updatePassword(c)) {
		assert.Equal(t, `{"status":"ok"}`, rec.Body.String())
		usersService.AssertCalled(t, "UpdatePassword", "test", "fuga")
	}
}

func TestLoginUser(t *testing.T) {
	e := echo.New()

	usersService := new(mocks.UsersService)
	usersService.On("AuthenticateWithPassword", "test_user", "test_pass").
		Return(true, nil)
	usersService.On("UpdateToken", "test_user").
		Return("new_token", nil)

	controller := &UsersController{
		UsersService: usersService,
	}

	req := httptest.NewRequest(echo.POST, "/users/_login",
		strings.NewReader(`{"user_id": "test_user", "password": "test_pass"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.loginUser(c)) {
		assert.Equal(t, `{"token":"new_token"}`, rec.Body.String())
	}
}
