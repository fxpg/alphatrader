package controllers

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

const (
	basePath = "/api"
)

type Server struct {
	UsersController     *UsersController     `inject:""`
	ChartController     *ChartController     `inject:""`
	ExchangesController *ExchangesController `inject:""`
	SettingsController  *SettingsController  `inject:""`
	TradersController   *TradersController   `inject:""`
}

func (c *Server) Serve(address string) error {
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.CORS())

	apis := e.Group(basePath)

	// Register controllers
	c.UsersController.Register(apis)
	c.ChartController.Register(apis)
	c.ExchangesController.Register(apis)
	c.SettingsController.Register(apis)
	c.TradersController.Register(apis)

	return e.Start(address)
}
