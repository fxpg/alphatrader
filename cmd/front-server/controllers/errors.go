package controllers

import (
	"net/http"
)

var (
	// ER0XX：General API Error
	// 入力値のJSONのパースに失敗した場合
	ER001 = Err(http.StatusBadRequest, 1, "invalid request json")
	ER002 = Err(http.StatusBadRequest, 2, "resource is not permitted")

	// ER1XX：User API Error
	ER101 = Err(http.StatusBadRequest, 101, "user already exists")
	ER102 = Err(http.StatusBadRequest, 102, "user_id is empty")
	ER103 = Err(http.StatusBadRequest, 103, "password is empty")
	ER104 = Err(http.StatusBadRequest, 104, "user_id is not valid")
	ER105 = Err(http.StatusBadRequest, 105, "password is not valid")
	ER106 = Err(http.StatusBadRequest, 106, "mismatch new_password and new_password2")
	ER107 = Err(http.StatusForbidden, 107, "not authorized")
	ER108 = Err(http.StatusBadRequest, 108, "user not found")
	ER109 = Err(http.StatusBadRequest, 109, "failed to login")
	ER110 = Err(http.StatusBadRequest, 110, "invalid input")
	ER111 = Err(http.StatusBadRequest, 111, "failed to update password")
	ER112 = Err(http.StatusBadRequest, 112, "failed to delete setting")
	ER113 = Err(http.StatusBadRequest, 113, "APIKey is empty")
	ER114 = Err(http.StatusBadRequest, 114, "SecretKey is empty")
	ER115 = Err(http.StatusBadRequest, 115, "same exchange api key has been already registered")

	// ER2XX：Chart API Error
	// ER3XX：Exchange API Error
	ER301 = Err(http.StatusBadRequest, 301, "invalid exchange id")

	// ER4XX：Backtest API Error
	// ER5XX：Trader API Error
	ER501 = Err(http.StatusBadRequest, 501, "make trader limit")
	ER502 = Err(http.StatusBadRequest, 502, "trader is not running")
	ER503 = Err(http.StatusBadRequest, 503, "trader is running. it cannot be removed")
	ER504 = Err(http.StatusBadRequest, 504, "trader is stopping. it cannot be removed")
)

type AppError struct {
	StatusCode int
	ErrorCode  int
	Message    string
}

func Err(statusCode int, errorCode int, message string) AppError {
	return AppError{
		StatusCode: statusCode,
		ErrorCode:  errorCode,
		Message:    message,
	}
}
