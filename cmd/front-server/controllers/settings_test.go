package controllers

import (
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types/mocks"
	"gitlab.com/fxpg/alphatrader/models"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestSettingsController_CreateSetting(t *testing.T) {
	e := echo.New()

	usersService := new(mocks.UsersService)
	settingsService := new(mocks.SettingsService)
	settingsService.On("Create", 123, models.ExchangeID(1),
		"api_key_1", "secret_key_1").
		Return(nil)

	controller := &SettingsController{
		UsersService:    usersService,
		SettingsService: settingsService,
	}

	req := httptest.NewRequest(echo.POST, "/settings",
		strings.NewReader(`
{"exchange_id": "1",
 "api_key": "api_key_1",
 "secret_key": "secret_key_1"}`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	req.Header.Set(HEADER_KEY_USERID, "123")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.CreateSetting(c)) {
		assert.Equal(t, `{"status":"ok"}`, rec.Body.String())
	}
	settingsService.AssertExpectations(t)
}

func TestSettingsController_GetSettings(t *testing.T) {
	e := echo.New()

	usersService := new(mocks.UsersService)
	settingsService := new(mocks.SettingsService)
	settingsService.On("FindAll", 123).
		Return([]*models.UserSetting{
			{
				ExchangeID: models.ExchangeID(1),
				UserID:     123,
				APIKey:     "api_key_1",
				SecretKey:  "secret_key_1",
			},
			{
				ExchangeID: models.ExchangeID(2),
				UserID:     123,
				APIKey:     "api_key_2",
				SecretKey:  "secret_key_2",
			},
		}, nil)

	controller := &SettingsController{
		UsersService:    usersService,
		SettingsService: settingsService,
	}

	req := httptest.NewRequest(echo.GET, "/settings", nil)
	req.Header.Set(HEADER_KEY_USERID, "123")
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.GetSettings(c)) {
		assert.Equal(t, `[{"exchange_id":1,"api_key":"api_key_1","secret_key":"secret_key_1"},{"exchange_id":2,"api_key":"api_key_2","secret_key":"secret_key_2"}]`,
			rec.Body.String())
	}
}
