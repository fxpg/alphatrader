package controllers

import (
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types/mocks"
	"strings"
)

func TestRunTrader(t *testing.T) {
	e := echo.New()
	group := e.Group("")

	tradersService := new(mocks.TradersService)
	tradersService.On("Run", mock.Anything, mock.Anything).Return(1, nil)

	controller := &TradersController{
		TradersService: tradersService,
	}
	controller.Register(group)

	req := httptest.NewRequest(echo.POST, "/traders",
		strings.NewReader(`{"tick_sec":1,"code":"print('ngo'"`))
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.runTrader(c)) {
		assert.Equal(t, `{"trader_id":1,"status":"running"}`, rec.Body.String())
		tradersService.AssertCalled(t, "Run", "hoge", 1, nil)
	}
}
