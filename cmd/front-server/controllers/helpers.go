package controllers

import (
	"github.com/labstack/echo"
	"strconv"
)

type errorResponse struct {
	Code    string `json:"error_code"`
	Message string `json:"error_message"`
}

func Error(c echo.Context, err AppError) error {
	res := errorResponse{
		Code:    strconv.Itoa(err.ErrorCode),
		Message: err.Message,
	}
	return c.JSON(err.StatusCode, res)
}
