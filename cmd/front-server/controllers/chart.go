package controllers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
	"gitlab.com/fxpg/alphatrader/models"
)

type ChartController struct {
	ChartService types.ChartService `inject:""`
}

func (ch *ChartController) Register(g *echo.Group) {
	g.GET("/charts", ch.getCharts)
}

func (ch *ChartController) getCharts(c echo.Context) error {
	exchangeIdStr := c.QueryParam("exchange_id")
	durationStr := c.QueryParam("duration")
	tradingStr := c.QueryParam("trading")
	settlementStr := c.QueryParam("settlement")
	startStr := c.QueryParam("start")
	endStr := c.QueryParam("end")

	exchangeId, err := strconv.Atoi(exchangeIdStr)
	if err != nil {
		return Error(c, ER001)
	}
	duration, err := strconv.Atoi(durationStr)
	if err != nil {
		return Error(c, ER001)
	}
	start, err := time.Parse(time.RFC3339, startStr)
	if err != nil {
		return Error(c, ER001)
	}
	end, err := time.Parse(time.RFC3339, endStr)
	if err != nil {
		return Error(c, ER001)
	}

	charts, err := ch.ChartService.Range(models.ExchangeID(exchangeId), duration,
		tradingStr, settlementStr, start, end)
	if err != nil {
		return err
	}

	type JSONChart struct {
		Last     float64   `json:"last,string"`
		Open     float64   `json:"open,string"`
		High     float64   `json:"high,string"`
		Low      float64   `json:"low,string"`
		Volume   float64   `json:"volume,string"`
		Datetime time.Time `json:"datetime"`
	}
	jsonCharts := make([]JSONChart, 0, len(charts))
	for _, chart := range charts {
		jsonCharts = append(jsonCharts, JSONChart{
			Last:     chart.Last,
			Open:     chart.Open,
			High:     chart.High,
			Low:      chart.Low,
			Volume:   chart.Volume,
			Datetime: chart.Datetime,
		})
	}

	return c.JSON(http.StatusOK, jsonCharts)
}
