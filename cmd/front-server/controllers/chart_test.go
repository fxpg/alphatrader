package controllers

import (
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types/mocks"
	"gitlab.com/fxpg/alphatrader/models"
	"net/http/httptest"
	"testing"
	"time"
)

func TestGetChartsReturnsEmptyArrayWhenThereIsNoCharts(t *testing.T) {
	e := echo.New()

	chartService := new(mocks.ChartService)
	chartService.On("Range", mock.Anything, mock.Anything,
		mock.Anything, mock.Anything, mock.Anything, mock.Anything).
		Return(make([]models.Chart, 0), nil)

	controller := &ChartController{
		ChartService: chartService,
	}

	req := httptest.NewRequest(echo.GET,
		"/charts?exchange_id=1&duration=60&trading=XRP&settlement=BTC&start=2017-12-01T13:34:19.322Z&end=2017-12-01T14:34:19.322Z",
		nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.getCharts(c)) {
		assert.Equal(t, `[]`, rec.Body.String())
	}
}

func TestGetChartsReturnsCharts(t *testing.T) {
	e := echo.New()

	charts := []models.Chart{
		{
			Last:     0.0001234,
			Open:     0.00001234,
			High:     0.123445,
			Low:      0.1234555,
			Volume:   123.123,
			Datetime: time.Date(2017, 10, 27, 12, 15, 57, 784, time.UTC),
		},
		{
			Last:     0.0001234,
			Open:     0.00001234,
			High:     0.123445,
			Low:      0.1234555,
			Volume:   123.123,
			Datetime: time.Date(2017, 10, 27, 12, 15, 57, 784, time.UTC),
		},
	}

	chartService := new(mocks.ChartService)
	chartService.On("Range", mock.Anything, mock.Anything,
		mock.Anything, mock.Anything,
		time.Date(2017, 12, 1, 13, 34, 19, 322000000, time.UTC),
		time.Date(2017, 12, 2, 14, 34, 19, 322000000, time.UTC)).
		Return(charts, nil)

	controller := &ChartController{
		ChartService: chartService,
	}

	req := httptest.NewRequest(echo.GET,
		"/charts?exchange_id=1&duration=60&trading=XRP&settlement=BTC&start=2017-12-01T13:34:19.322Z&end=2017-12-02T14:34:19.322Z",
		nil)
	rec := httptest.NewRecorder()
	c := e.NewContext(req, rec)

	if assert.NoError(t, controller.getCharts(c)) {
		assert.Equal(t, `[{"last":"0.0001234","open":"0.00001234","high":"0.123445","low":"0.1234555","volume":"123.123","datetime":"2017-10-27T12:15:57.000000784Z"},{"last":"0.0001234","open":"0.00001234","high":"0.123445","low":"0.1234555","volume":"123.123","datetime":"2017-10-27T12:15:57.000000784Z"}]`, rec.Body.String())
	}
}
