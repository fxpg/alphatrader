package controllers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
)

type ExchangesController struct {
	ExchangesService types.ExchangesService `inject:""`
}

func (e *ExchangesController) Register(g *echo.Group) {
	g.GET("/exchanges", e.getExchanges)
	g.GET("/exchanges/:exchange_id/currency_pairs", e.getCurrencyPairs)
}

func (e *ExchangesController) getCurrencyPairs(c echo.Context) error {
	exchangeId, err := strconv.Atoi(c.Param("exchange_id"))
	if err != nil {
		return Error(c, ER301)
	}
	currencyPairs := e.ExchangesService.GetCurrencyPairsByExchangeID(exchangeId)
	return c.JSON(http.StatusOK, currencyPairs)
}

func (e *ExchangesController) getExchanges(c echo.Context) error {
	exchanges := e.ExchangesService.GetExchanges()

	return c.JSON(http.StatusOK, exchanges)
}
