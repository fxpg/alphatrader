package controllers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
	"gitlab.com/fxpg/alphatrader/models"
)

type SettingsController struct {
	UsersService    types.UsersService    `inject:""`
	SettingsService types.SettingsService `inject:""`
}

func (s *SettingsController) Register(g *echo.Group) {
	g.GET("/settings", s.GetSettings, AuthV2(s.UsersService))
	g.POST("/settings", s.CreateSetting, AuthV2(s.UsersService))
	g.DELETE("/settings/:setting_id", s.DeleteSetting, AuthV2(s.UsersService))
}

func (s *SettingsController) CreateSetting(c echo.Context) error {
	userId := c.Request().Header.Get(HEADER_KEY_USERID)

	var in struct {
		ExchangeId int    `json:"exchange_id"`
		ApiKey     string `json:"api_key"`
		SecretKey  string `json:"secret_key"`
	}
	if err := c.Bind(&in); err != nil {
		return Error(c, ER001)
	}

	user, err := s.UsersService.FindByUserId(userId)
	if err != nil {
		return Error(c, ER103)
	}
	isExist, err := s.SettingsService.IsExist(int(user.ID), models.ExchangeID(int(in.ExchangeId)))
	if isExist {
		return Error(c, ER115)
	}
	if err != nil {
		return err
	}

	if in.ApiKey == "" {
		return Error(c, ER113)
	}
	if in.SecretKey == "" {
		return Error(c, ER114)
	}
	err = s.SettingsService.Create(int(user.ID), models.ExchangeID(int(in.ExchangeId)),
		in.ApiKey, in.SecretKey)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		Status string `json:"status"`
	}{"ok"})
}

func (s *SettingsController) DeleteSetting(c echo.Context) error {
	userId := c.Request().Header.Get(HEADER_KEY_USERID)

	user, err := s.UsersService.FindByUserId(userId)
	if err != nil {
		return err
	}
	settingId, err := strconv.Atoi(c.Param("setting_id"))
	if err != nil {
		return Error(c, ER001)
	}

	deleted, err := s.SettingsService.DeleteUserSetting(int(user.ID), settingId)
	if err != nil {
		return err
	}
	if !deleted {
		return Error(c, ER112)
	}

	return c.JSON(http.StatusOK, struct {
		Status string `json:"status"`
	}{"ok"})
}

func (s *SettingsController) GetSettings(c echo.Context) error {
	userId := c.Request().Header.Get(HEADER_KEY_USERID)

	user, err := s.UsersService.FindByUserId(userId)
	if err != nil {
		return err
	}
	if user == nil {
		return Error(c, ER107)
	}
	settings, err := s.SettingsService.FindAll(int(user.ID))
	if err != nil {
		return err
	}

	type outSetting struct {
		SettingID  int    `json:"setting_id"`
		ExchangeId int    `json:"exchange_id"`
		ApiKey     string `json:"api_key"`
	}
	out := make([]outSetting, 0)
	for _, setting := range settings {
		out = append(out, outSetting{
			SettingID:  int(setting.ID),
			ExchangeId: int(setting.ExchangeID),
			ApiKey:     setting.APIKey,
		})
	}
	return c.JSON(http.StatusOK, out)
}
