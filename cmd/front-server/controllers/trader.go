package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
)

type TradersController struct {
	UsersService   types.UsersService   `inject:""`
	OrdersService  types.OrdersService  `inject:""`
	TradersService types.TradersService `inject:""`
	GrpcService    types.GrpcService    `inject:""`
}

func (b *TradersController) Register(g *echo.Group) {
	g.POST("/traders", b.runTrader, AuthV2(b.UsersService))
	g.GET("/traders", b.getTraders, AuthV2(b.UsersService))

	g.POST("/traders/:trader_id/stop", b.queueStopTrader, AuthV2(b.UsersService))
	g.DELETE("/traders/:trader_id", b.deleteTrader, AuthV2(b.UsersService))
	g.GET("/traders/:trader_id", b.getTrader, AuthV2(b.UsersService))
	g.GET("/traders/:trader_id/orders", b.getOrders, AuthV2(b.UsersService))
}

func (b *TradersController) deleteTrader(c echo.Context) error {
	userIdStr := c.Request().Header.Get(HEADER_KEY_USERID)
	user, err := b.UsersService.FindByUserId(userIdStr)

	if err != nil {
		return Error(c, ER107)
	}

	traderId, err := strconv.Atoi(c.Param("trader_id"))
	if err != nil {
		fmt.Println(err)
		return Error(c, ER001)
	}

	trader, err := b.TradersService.GetTrader(traderId)
	if err != nil {
		return err
	}
	if trader.UserID != int(user.ID) {
		return Error(c, ER002)
	}

	if trader.Status == "running" {
		return Error(c, ER503)
	}
	if trader.Status == "stopping" {
		return Error(c, ER504)
	}

	err = b.TradersService.Delete(int(trader.ID))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		Status string `json:"status"`
	}{"ok"})
}

func (b *TradersController) queueStopTrader(c echo.Context) error {
	userIdStr := c.Request().Header.Get(HEADER_KEY_USERID)
	user, err := b.UsersService.FindByUserId(userIdStr)

	if err != nil {
		return Error(c, ER107)
	}

	traderId, err := strconv.Atoi(c.Param("trader_id"))
	if err != nil {
		fmt.Println(err)
		return Error(c, ER001)
	}

	trader, err := b.TradersService.GetTrader(traderId)
	if err != nil {
		return err
	}
	if trader.UserID != int(user.ID) {
		return Error(c, ER002)
	}

	if trader.Status != "running" {
		return Error(c, ER502)
	}

	err = b.TradersService.ChangeStatus(traderId, "stopping")
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		Status string `json:"status"`
	}{"ok"})
}

func (b *TradersController) runTrader(c echo.Context) error {
	userIdStr := c.Request().Header.Get(HEADER_KEY_USERID)
	user, err := b.UsersService.FindByUserId(userIdStr)

	if err != nil {
		return err
	}

	var in struct {
		Description string `json:"description"`
		Code        string `json:"code"`
		TickSec     int    `json:"tick_sec"`
	}
	if err := c.Bind(&in); err != nil {
		return Error(c, ER001)
	}

	var nOfTrader int
	switch user.Plan {
	case "free":
		nOfTrader = 2
	case "pro":
		nOfTrader = 5
	default:
		nOfTrader = 3
	}
	traders, err := b.TradersService.GetTraders(int(user.ID))
	if err != nil {
		return err
	}
	if len(traders)+1 > nOfTrader {
		return Error(c, ER501)
	}

	_, err = b.GrpcService.GenerateResourceToken(int(user.ID))
	if err != nil {
		return err
	}
	setting := &types.TraderSetting{
		Description: in.Description,
		UserId:      int(user.ID),
		TickSec:     in.TickSec,
		Code:        in.Code,
	}

	traderId, err := b.TradersService.Set(setting)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, struct {
		TraderId int    `json:"trader_id"`
		Status   string `json:"status"`
	}{
		TraderId: traderId,
		Status:   "running",
	})
}

func (b *TradersController) getTraders(c echo.Context) error {
	userIdStr := c.Request().Header.Get(HEADER_KEY_USERID)
	user, err := b.UsersService.FindByUserId(userIdStr)

	if err != nil {
		return Error(c, ER107)
	}

	traders, err := b.TradersService.GetTraders(int(user.ID))
	if err != nil {
		return err
	}
	type traderStatus struct {
		TraderID        int    `json:"trader_id"`
		TickSec         int    `json:"tick_sec"`
		Description     string `json:"description"`
		Status          string `json:"status"`
		TerminateReason string `json:"terminate_reason"`
	}

	traderSs := make([]*traderStatus, 0)
	for _, trader := range traders {
		traderSs = append(traderSs, &traderStatus{
			TraderID:        int(trader.ID),
			TickSec:         trader.TickSec,
			Description:     trader.Description,
			Status:          trader.Status,
			TerminateReason: trader.TerminateReason,
		})
	}

	return c.JSON(http.StatusOK, traderSs)
}

func (b *TradersController) getTrader(c echo.Context) error {
	userIdStr := c.Request().Header.Get(HEADER_KEY_USERID)
	user, err := b.UsersService.FindByUserId(userIdStr)
	if err != nil {
		return Error(c, ER107)
	}

	traderId, err := strconv.Atoi(c.Param("trader_id"))
	if err != nil {
		fmt.Println(err)
		return Error(c, ER001)
	}

	trader, err := b.TradersService.GetTrader(traderId)
	if err != nil {
		return err
	}
	if trader.UserID != int(user.ID) {
		return Error(c, ER002)
	}

	return c.JSON(http.StatusOK, struct {
		TraderId        int    `json:"trader_id"`
		Description     string `json:"description"`
		Code            string `json:"code"`
		TickSec         int    `json:"tick_sec"`
		Status          string `json:"status"`
		TerminateReason string `json:"terminate_reason"`
	}{
		TraderId:        traderId,
		Description:     trader.Description,
		Code:            trader.Code,
		TickSec:         trader.TickSec,
		Status:          trader.Status,
		TerminateReason: trader.TerminateReason,
	})
}

func (b *TradersController) getOrders(c echo.Context) error {
	userIdStr := c.Request().Header.Get(HEADER_KEY_USERID)
	user, err := b.UsersService.FindByUserId(userIdStr)

	if err != nil {
		fmt.Println(err)
		return Error(c, ER107)
	}

	traderId, err := strconv.Atoi(c.Param("trader_id"))
	if err != nil {
		return Error(c, ER001)
	}

	trader, err := b.TradersService.GetTrader(traderId)
	if err != nil {
		return err

	}
	if trader.UserID != int(user.ID) {
		return Error(c, ER002)
	}

	orders, err := b.OrdersService.FindByTraderId(traderId)
	if err != nil {
		return err
	}

	type ord struct {
		OrderID         uint    `json:"order_id"`
		Exchange        string  `json:"exchange_id"`
		Pair            string  `json:"pair"`
		ExchangeOrderId string  `json:"exchange_order_id"`
		Datetime        string  `json:"datetime"`
		Status          bool    `json:"status"`
		Price           float64 `json:"price"`
		Amount          float64 `json:"amount"`
		TradeType       string  `json:"trade_type"`
	}

	ords := make([]*ord, 0)
	for _, order := range orders {
		ords = append(ords, &ord{
			OrderID:         order.ID,
			Exchange:        order.ExchangeID.String(),
			Pair:            order.Pair,
			ExchangeOrderId: order.OrderID,
			Datetime:        order.Datetime.String(),
			Status:          order.Status,
			Price:           order.Price,
			Amount:          order.Amount,
			TradeType:       order.TradeType.String(),
		})
	}

	return c.JSON(http.StatusOK, ords)
}
