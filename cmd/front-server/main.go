package main

import (
	"fmt"
	"os"
	"strconv"

	"github.com/facebookgo/inject"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	controllers2 "gitlab.com/fxpg/alphatrader/cmd/front-server/controllers"
	"gitlab.com/fxpg/alphatrader/config"
	"gitlab.com/fxpg/alphatrader/models"
	"gitlab.com/fxpg/alphatrader/repositories"
	"gitlab.com/fxpg/alphatrader/services"
)

func help_and_exit() {
	fmt.Fprintf(os.Stderr, "%s config.yml\n", os.Args[0])
	os.Exit(1)
}

func migrateDB(db *gorm.DB) {
	models.Migrate(db)
}

var ExchangeIDs = []models.ExchangeID{
	models.Bitflyer,
	models.Poloniex,
	models.Hitbtc,
}

func main() {
	if len(os.Args) != 2 {
		help_and_exit()
	}
	confPath := os.Args[1]
	conf := config.ReadConfig(confPath)
	fmt.Printf("DEBUG:" + strconv.FormatBool(conf.Debug))

	// open db
	db, err := gorm.Open("mysql", conf.DBConnection)
	db.LogMode(false)
	if err != nil {
		panic(errors.Wrap(err, "failed to open db"))
	}
	migrateDB(db)
	if err := models.Migrate(db); err != nil {
		panic(err)
	}
	gsrepo := services.GrpcServerRepository(&repositories.GrpcServerRepositoryEC2{})
	if conf.Debug {
		gsrepo = services.GrpcServerRepository(&repositories.GrpcServerRepositoryLocal{})
	}

	// init DI
	var server controllers2.Server
	var g inject.Graph
	err = g.Provide(
		&inject.Object{Value: db},
		&inject.Object{Value: conf},

		// Repositories
		&inject.Object{Value: &repositories.UserRepositoryGorm{}},
		&inject.Object{Value: &repositories.UserSettingsRepositoryGorm{}},
		&inject.Object{Value: &repositories.ChartRepositoryGorm{}},
		&inject.Object{Value: &repositories.ResourceTokenRepositoryGorm{}},
		&inject.Object{Value: &repositories.TraderRepositoryGorm{}},
		&inject.Object{Value: repositories.NewExchangeRepositoryFromExchangeAPI(ExchangeIDs)},
		&inject.Object{Value: &repositories.OrderRepositoryGorm{}},
		&inject.Object{Value: gsrepo},

		// Services
		&inject.Object{Value: &services.UsersServiceImpl{}},
		&inject.Object{Value: &services.ChartServiceImpl{}},
		&inject.Object{Value: &services.ExchangesServiceImpl{}},
		&inject.Object{Value: &services.OrdersServiceImpl{}},
		&inject.Object{Value: &services.SettingsServiceImpl{}},
		&inject.Object{Value: &services.TradersServiceImpl{}},
		&inject.Object{Value: &services.GrpcServiceImpl{}},

		// Controllers
		&inject.Object{Value: &controllers2.TradersController{}},
		&inject.Object{Value: &controllers2.UsersController{}},
		&inject.Object{Value: &controllers2.ChartController{}},
		&inject.Object{Value: &controllers2.ExchangesController{}},
		&inject.Object{Value: &controllers2.SettingsController{}},

		// Server
		&inject.Object{Value: &server},
	)
	if err != nil {
		panic(err)
	}
	if err := g.Populate(); err != nil {
		panic(err)
	}

	//
	panic(server.Serve(":8080"))

}
