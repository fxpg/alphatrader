package main

import (
	"fmt"
	"github.com/facebookgo/inject"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/config"
	"gitlab.com/fxpg/alphatrader/logger"
	"gitlab.com/fxpg/alphatrader/models"
	"gitlab.com/fxpg/alphatrader/repositories"
	"gitlab.com/fxpg/alphatrader/services"
	"os"
)

func help_and_exit() {
	fmt.Fprintf(os.Stderr, "%s config.yml\n", os.Args[0])
	os.Exit(1)
}

func main() {
	if len(os.Args) != 2 {
		help_and_exit()
	}
	confPath := os.Args[1]
	conf := config.ReadConfig(confPath)

	db, err := gorm.Open("mysql", conf.DBConnection)
	db.LogMode(false)
	if err != nil {
		panic(errors.Wrap(err, "failed to open db"))
	}
	if err := models.Migrate(db); err != nil {
		panic(err)
	}

	gsrepo := services.GrpcServerRepository(&repositories.GrpcServerRepositoryEC2{})
	if conf.Debug {
		gsrepo = services.GrpcServerRepository(&repositories.GrpcServerRepositoryLocal{})
	}
	server := Server{}
	// init DI
	var g inject.Graph
	err = g.Provide(
		&inject.Object{Value: db},
		&inject.Object{Value: conf},

		// Repositories
		&inject.Object{Value: &repositories.ChartRepositoryGorm{}},
		&inject.Object{Value: &repositories.OrderRepositoryGorm{}},
		&inject.Object{Value: &repositories.UserRepositoryGorm{}},
		&inject.Object{Value: &repositories.TraderRepositoryGorm{}},
		&inject.Object{Value: &repositories.ResourceTokenRepositoryGorm{}},
		&inject.Object{Value: gsrepo},

		// Services
		&inject.Object{Value: &services.TradersServiceImpl{}},

		// Server
		&inject.Object{Value: &server},
	)
	if err != nil {
		panic(err)
	}
	if err := g.Populate(); err != nil {
		panic(err)
	}
	logger.Get().Info("starting job_server...")
	server.Run()
}
