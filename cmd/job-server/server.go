package main

import (
	"time"

	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
	"gitlab.com/fxpg/alphatrader/logger"
)

const (
	D1M  = time.Minute * 1
	D5M  = time.Minute * 5
	D10M = time.Minute * 10
	D30M = time.Minute * 30
	D1H  = time.Hour * 1
	D3H  = time.Hour * 3
	D6H  = time.Hour * 6
	D12H = time.Hour * 12
	D24H = time.Hour * 24
)

type Server struct {
	TradersService types.TradersService `inject:""`
}

func (s *Server) Run() {
	t1m := time.NewTicker(D1M)
	t5m := time.NewTicker(D5M)
	t10m := time.NewTicker(D10M)
	t30m := time.NewTicker(D30M)
	t1h := time.NewTicker(D1H)
	t3h := time.NewTicker(D3H)
	t6h := time.NewTicker(D6H)
	t12h := time.NewTicker(D12H)
	t24h := time.NewTicker(D24H)
	for {
		select {
		case <-t1m.C:
			logger.Get().Info("heartbeat 1m trader...")
			s.HeartBeat(D1M)
		case <-t5m.C:
			logger.Get().Info("heartbeat 5m trader...")
			s.HeartBeat(D5M)
		case <-t10m.C:
			logger.Get().Info("heartbeat 10m trader...")
			s.HeartBeat(D10M)
		case <-t30m.C:
			logger.Get().Info("heartbeat 30m trader...")
			s.HeartBeat(D30M)
		case <-t1h.C:
			logger.Get().Info("heartbeat 60m trader...")
			s.HeartBeat(D1H)
		case <-t3h.C:
			logger.Get().Info("heartbeat 180m trader...")
			s.HeartBeat(D3H)
		case <-t6h.C:
			logger.Get().Info("heartbeat 360m trader...")
			s.HeartBeat(D6H)
		case <-t12h.C:
			logger.Get().Info("heartbeat 720m trader...")
			s.HeartBeat(D12H)
		case <-t24h.C:
			logger.Get().Info("heartbeat 1440m trader...")
			s.HeartBeat(D24H)
		}
	}
	t1m.Stop()
	t5m.Stop()
	t10m.Stop()
	t30m.Stop()
	t1h.Stop()
	t3h.Stop()
	t6h.Stop()
	t12h.Stop()
	t24h.Stop()
}

func (s *Server) HeartBeat(duration time.Duration) {
	err := s.TradersService.ChangeStoppingToStopped()
	if err != nil {
		logger.Get().Info(err)
	}
	traders, err := s.TradersService.GetTradersByDuration(int(duration.Seconds()))
	if err != nil {
		logger.Get().Info(err)
		return
	}

	for _, trader := range traders {
		err := s.TradersService.Run(int(trader.ID))
		if err != nil {
			logger.Get().Info(err)
		}
	}
	return
}
