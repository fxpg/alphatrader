package main

import (
	sapi "gitlab.com/fxpg/alphatrader/serviceapi"
)

type watcherMap map[string]map[string]*PairWatcher

func (m watcherMap) keys() []sapi.CurrencyPair {
	keys := make([]sapi.CurrencyPair, 0, len(m)*3)
	for trading, sm := range m {
		for settlement := range sm {
			keys = append(keys, sapi.CurrencyPair{Trading: trading, Settlement: settlement})
		}
	}
	return keys
}

func (m watcherMap) get(pair *sapi.CurrencyPair) (*PairWatcher, bool) {
	if sm, ok := m[pair.Trading]; !ok {
		return nil, false
	} else if w, ok := sm[pair.Settlement]; !ok {
		return nil, false
	} else {
		return w, true
	}
}

func (m watcherMap) put(pair *sapi.CurrencyPair, watcher *PairWatcher) {
	sm, ok := m[pair.Trading]
	if !ok {
		sm = make(map[string]*PairWatcher)
		m[pair.Trading] = sm
	}

	sm[pair.Settlement] = watcher
}
