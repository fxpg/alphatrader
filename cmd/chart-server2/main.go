package main

import (
	"fmt"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/config"
	"gitlab.com/fxpg/alphatrader/logger"
	"gitlab.com/fxpg/alphatrader/models"
	sapi "gitlab.com/fxpg/alphatrader/serviceapi"
)

func help_and_exit() {
	fmt.Fprintf(os.Stderr, "%s config.yml\n", os.Args[0])
	os.Exit(1)
}

var ExchangeIDs = []models.ExchangeID{
	models.Bitflyer,
	models.Hitbtc,
	models.Poloniex,
}

func main() {
	if len(os.Args) != 2 {
		help_and_exit()
	}
	confPath := os.Args[1]

	conf := config.ReadConfig(confPath)
	var eapis []sapi.ExchangeApi
	for _, exchangeId := range ExchangeIDs {
		ea, err := sapi.NewExchangeAPI(exchangeId)
		if err != nil {
			logger.Get().Info(err)
			continue
		}
		eapis = append(eapis, ea)

	}

	db, err := gorm.Open("mysql", conf.DBConnection)
	if err != nil {
		panic(errors.Wrap(err, "failed to connect db"))
	}
	db.AutoMigrate(&models.Chart{})

	server := NewServer(eapis, db)
	logger.Get().Info("starting chart_server...")
	server.Run()
}
