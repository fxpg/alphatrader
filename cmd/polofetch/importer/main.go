package main

import (
	"github.com/gocarina/gocsv"
	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/alphatrader/cmd/polofetch/model"
	"gitlab.com/fxpg/alphatrader/models"
	"log"
	"os"
	"time"
)

func main() {
	db, err := gorm.Open("mysql", "fxpg:sHD9xDkHPdTf@tcp(localhost:3306)/alphatrader?charset=utf8&parseTime=True&loc=UTC")
	if err != nil {
		log.Fatal(err)
	}

	var chartDatum []model.ChartData
	if err := gocsv.UnmarshalFile(os.Stdin, &chartDatum); err != nil {
		log.Fatal(err)
	}

	for _, chartData := range chartDatum {
		chart := models.Chart{
			Datetime:   time.Unix(int64(chartData.Date), 0),
			Last:       chartData.Close,
			Open:       chartData.Open,
			High:       chartData.High,
			Low:        chartData.Low,
			Volume:     chartData.Volume,
			Duration:   300,
			Pair:       "BTC_XRP",
			ExchangeID: 2,
		}
		if err := db.Create(&chart).Error; err != nil {
			log.Fatal(err)
		}
	}
}
