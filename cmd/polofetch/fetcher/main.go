package main

import (
	"encoding/json"
	"fmt"
	"github.com/gocarina/gocsv"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/cmd/polofetch/model"
	"log"
	"net/http"
	"time"
)

const (
	urlBase = "https://poloniex.com/public?command=returnChartData"
)

func fetchChartDatum(currencyPair string, start time.Time, end time.Time, periodSec int) ([]model.ChartData, error) {
	url := fmt.Sprintf("%s&currencyPair=%s&start=%d&end=%d&period=%d",
		urlBase, currencyPair, start.Unix(), end.Unix(), periodSec)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var chartDatum []model.ChartData
	if err := json.NewDecoder(resp.Body).Decode(&chartDatum); err != nil {
		return nil, errors.Wrap(err, "cannot unmarshal json")
	}

	return chartDatum, nil
}

func main() {
	currencyPair := "BTC_XRP"
	end := time.Now().AddDate(0, 0, 0)
	start := end.AddDate(-1, 0, 0)

	chartDatum, err := fetchChartDatum(currencyPair, start, end, 300)
	if err != nil {
		log.Fatal(err)
	}
	str, err := gocsv.MarshalString(chartDatum)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(str)
}
