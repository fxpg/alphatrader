package controllers

import (
	pb "gitlab.com/fxpg/alphatrader/atgrpc/front_server"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
	"gitlab.com/fxpg/alphatrader/models"
	"golang.org/x/net/context"

	"strings"
	"time"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
)

type Server struct {
	ChartService    types.ChartService    `inject:""`
	OrdersService   types.OrdersService   `inject:""`
	GrpcService     types.GrpcService     `inject:""`
	TraderService   types.TradersService  `inject:""`
	SettingsService types.SettingsService `inject:""`
}

func (s *Server) Tick(ctx context.Context, in *pb.TickFrontRequest) (*pb.TickFrontResponse, error) {
	exchangeStr := in.Exchange
	duration := in.Duration
	pairStr := in.Pair
	settleTrading := strings.Split(pairStr, "_")
	exchangeId, err := models.ExchangeIDString(strings.Title(exchangeStr))
	if err != nil {
		return &pb.TickFrontResponse{}, err
	}
	tick, err := s.ChartService.Find(exchangeId, int(duration), settleTrading[0], settleTrading[1])

	return &pb.TickFrontResponse{
		Open:   float32(tick.Open),
		Close:  float32(tick.Last),
		High:   float32(tick.High),
		Low:    float32(tick.Low),
		Volume: float32(tick.Volume),

		Timestamp: tick.Datetime.Unix(),
	}, nil
}
func (s *Server) Charts(ctx context.Context, in *pb.ChartsFrontRequest) (*pb.ChartsFrontResponse, error) {
	exchangeStr := in.Exchange
	duration := in.Duration
	pairStr := in.Pair
	num := in.Num
	settleTrading := strings.Split(pairStr, "_")
	exchangeId, err := models.ExchangeIDString(strings.Title(exchangeStr))
	if err != nil {
		return &pb.ChartsFrontResponse{}, err
	}

	charts, err := s.ChartService.FindN(exchangeId, int(duration), settleTrading[0], settleTrading[1], int(num))

	cs := make([]*pb.TickFrontResponse, 0, len(charts))
	for _, chart := range charts {
		cs = append(cs, &pb.TickFrontResponse{
			Close:     float32(chart.Last),
			Open:      float32(chart.Open),
			High:      float32(chart.High),
			Low:       float32(chart.Low),
			Volume:    float32(chart.Volume),
			Timestamp: chart.Datetime.Unix(),
		})
	}

	return &pb.ChartsFrontResponse{
		Ticks: cs,
	}, nil
}

func (s *Server) InsertOrderData(ctx context.Context, in *pb.InsertOrderDataRequest) (*pb.InsertOrderDataResponse, error) {
	orderType := models.TradeType(in.OrderType)
	err := s.OrdersService.Insert(int(in.UserId), int(in.TraderId), time.Unix(in.Datetime, 0), in.Exchange, in.Pair, in.OrderId, float64(in.Amount), float64(in.Price), orderType)
	if err != nil {
		return &pb.InsertOrderDataResponse{
			Completed: false,
		}, err
	}
	return &pb.InsertOrderDataResponse{
		Completed: true,
	}, nil
}
func (s *Server) Auth(ctx context.Context, in *pb.AuthRequest) (*pb.AuthResponse, error) {
	isValid, err := s.GrpcService.Auth(int(in.UserId), in.Token)
	if err != nil {
		return &pb.AuthResponse{
			Isvalid: false,
		}, err
	}
	return &pb.AuthResponse{
		Isvalid: isValid,
	}, nil
}

func (s *Server) Error(ctx context.Context, in *pb.ErrorFrontRequest) (*empty.Empty, error) {
	if in.Error == pb.ErrorFrontRequest_Timeout {
		return &empty.Empty{}, s.TraderService.Terminate(int(in.TraderId), "timeout")

	}
	if in.Error == pb.ErrorFrontRequest_CompileFailed {
		return &empty.Empty{}, s.TraderService.Terminate(int(in.TraderId), "compile failed")

	}
	if in.Error == pb.ErrorFrontRequest_Others {
		return &empty.Empty{}, s.TraderService.Terminate(int(in.TraderId), "other errors")
	}
	return &empty.Empty{}, nil
}

func (s *Server) APIkey(ctx context.Context, in *pb.APIkeyRequest) (*pb.APIkeyResponse, error) {
	settings, err := s.SettingsService.FindAll(int(in.UserId))
	if err != nil {
		return &pb.APIkeyResponse{}, err
	}
	for _, setting := range settings {
		if strings.ToLower(setting.ExchangeID.String()) == in.Exchange {
			return &pb.APIkeyResponse{
				ApiKey:    setting.APIKey,
				SecretKey: setting.SecretKey,
			}, nil
		}
	}
	return &pb.APIkeyResponse{}, errors.New("exchange setting is not found")
}
