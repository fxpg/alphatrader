package main

import (
	"fmt"
	"github.com/facebookgo/inject"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	pb "gitlab.com/fxpg/alphatrader/atgrpc/front_server"
	"gitlab.com/fxpg/alphatrader/cmd/grpc-server/controllers"
	"gitlab.com/fxpg/alphatrader/config"
	"gitlab.com/fxpg/alphatrader/models"
	"gitlab.com/fxpg/alphatrader/repositories"
	"gitlab.com/fxpg/alphatrader/services"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
	"os"
)

const (
	port = ":50051"
)

func help_and_exit() {
	fmt.Fprintf(os.Stderr, "%s config.yml\n", os.Args[0])
	os.Exit(1)
}

func migrateDB(db *gorm.DB) {
	models.Migrate(db)
}

func main() {
	if len(os.Args) != 2 {
		help_and_exit()
	}
	confPath := os.Args[1]
	conf := config.ReadConfig(confPath)

	lis, err := net.Listen("tcp", port)
	if err != nil {
		panic(errors.Wrapf(err, "failed to listen %v", err))
	}
	db, err := gorm.Open("mysql", conf.DBConnection)
	db.LogMode(false)
	if err != nil {
		panic(errors.Wrap(err, "failed to open db"))
	}
	if err := models.Migrate(db); err != nil {
		panic(err)
	}

	gsrepo := services.GrpcServerRepository(&repositories.GrpcServerRepositoryEC2{})
	if conf.Debug {
		gsrepo = services.GrpcServerRepository(&repositories.GrpcServerRepositoryLocal{})
	}
	// init DI
	var server controllers.Server
	var g inject.Graph
	err = g.Provide(
		&inject.Object{Value: db},
		&inject.Object{Value: conf},

		// Repositories
		&inject.Object{Value: &repositories.ChartRepositoryGorm{}},
		&inject.Object{Value: &repositories.OrderRepositoryGorm{}},
		&inject.Object{Value: &repositories.UserRepositoryGorm{}},
		&inject.Object{Value: &repositories.UserSettingsRepositoryGorm{}},
		&inject.Object{Value: &repositories.TraderRepositoryGorm{}},
		&inject.Object{Value: &repositories.ResourceTokenRepositoryGorm{}},
		&inject.Object{Value: gsrepo},

		// Services
		&inject.Object{Value: &services.ChartServiceImpl{}},
		&inject.Object{Value: &services.OrdersServiceImpl{}},
		&inject.Object{Value: &services.TradersServiceImpl{}},
		&inject.Object{Value: &services.GrpcServiceImpl{}},
		&inject.Object{Value: &services.SettingsServiceImpl{}},

		// Server
		&inject.Object{Value: &server},
	)
	if err != nil {
		panic(err)
	}
	if err := g.Populate(); err != nil {
		panic(err)
	}
	s := grpc.NewServer()
	pb.RegisterFrontServerServer(s, &server)

	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		panic(errors.Wrapf(err, "failed to serve %v", err))
	}
}
