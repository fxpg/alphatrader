package services

import (
	"crypto/sha256"
	"encoding/base64"

	"github.com/labstack/gommon/random"
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/models"
)

type UsersServiceImpl struct {
	UserRepository        UserRepository        `inject:""`
	UserSettingRepository UserSettingRepository `inject:""`
}

func (s *UsersServiceImpl) IsExists(userId string) (bool, error) {
	user, err := s.UserRepository.FindByStrId(userId)
	if err != nil {
		return false, errors.Wrap(err, "failed to find user")
	}

	if user == nil {
		return false, nil
	}

	return true, nil
}

func hashPassword(password string) string {
	hashed := sha256.Sum256([]byte(password))
	return base64.StdEncoding.EncodeToString(hashed[:])
}

func (s *UsersServiceImpl) Create(userId string, password string) error {
	user := &models.User{
		UserID:   userId,
		Password: hashPassword(password),
		Token:    nil,
		Plan:     "free",
	}

	if err := s.UserRepository.Create(user); err != nil {
		return errors.Wrap(err, "failed to create user")
	}
	return nil
}

func (s *UsersServiceImpl) Authenticate(userId string, token string) (bool, error) {
	user, err := s.UserRepository.FindByStrId(userId)
	if err != nil {
		return false, errors.Wrapf(err, "failed to find user, userId=%s", userId)
	}
	if user == nil {
		return false, nil
	}
	if user.Token != nil && *user.Token == token {
		return true, nil
	}

	return false, nil
}

func (s *UsersServiceImpl) AuthenticateWithPassword(userId string, password string) (bool, error) {
	user, err := s.UserRepository.FindByStrId(userId)
	if err != nil {
		return false, errors.Wrapf(err, "failed to find user, userId=%s", userId)
	}
	if user == nil {
		return false, nil
	}

	if user.Password == hashPassword(password) {
		return true, nil
	}

	return false, nil
}

func (s *UsersServiceImpl) FindByUserId(userId string) (*models.User, error) {
	user, err := s.UserRepository.FindByStrId(userId)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to find user, userId=%s", userId)
	}
	return user, nil
}

func (s *UsersServiceImpl) FindUsersExchange(userId int) ([]models.ExchangeID, error) {
	settings, err := s.UserSettingRepository.FindByUserId(userId)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to find user_settings, userId=%s", userId)
	}

	exchangeIDs := make([]models.ExchangeID, 0)
	for _, setting := range settings {
		exchangeIDs = append(exchangeIDs, setting.ExchangeID)
	}

	return exchangeIDs, nil
}

func (s *UsersServiceImpl) UpdateToken(userId string) (string, error) {
	user, err := s.UserRepository.FindByStrId(userId)
	if err != nil {
		return "", errors.Wrap(err, "failed to find user")
	}
	if user == nil {
		return "", errors.New("user not found")
	}

	newToken := random.New().String(30)
	user.Token = &newToken
	if err := s.UserRepository.UpdateToken(user); err != nil {
		return "", errors.Wrap(err, "failed to update token")
	}

	return newToken, nil
}

func (s *UsersServiceImpl) UpdatePassword(userId string, newPassword string) error {
	user, err := s.UserRepository.FindByStrId(userId)
	if err != nil {
		return errors.Wrap(err, "failed to find user")
	}
	if user == nil {
		return errors.New("user not found")
	}

	user.Password = newPassword
	if err := s.UserRepository.UpdatePassword(user); err != nil {
		return errors.Wrap(err, "failed to update password")
	}

	return nil
}
