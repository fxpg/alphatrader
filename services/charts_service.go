package services

import (
	"time"

	"gitlab.com/fxpg/alphatrader/models"
)

type ChartServiceImpl struct {
	ChartRepository ChartRepository `inject:""`
}

func (c *ChartServiceImpl) Range(exchangeId models.ExchangeID, duration int,
	trading string, settlement string,
	start time.Time, end time.Time) ([]models.Chart, error) {

	return c.ChartRepository.FindRange(exchangeId, duration, trading, settlement, start, end)
}

func (c *ChartServiceImpl) FindN(exchangeId models.ExchangeID, duration int,
	trading string, settlement string, num int) ([]models.Chart, error) {

	return c.ChartRepository.FindN(exchangeId, duration, trading, settlement, num)
}

func (c *ChartServiceImpl) Find(exchangeId models.ExchangeID, duration int,
	trading string, settlement string) (models.Chart, error) {
	return c.ChartRepository.Find(exchangeId, duration, trading, settlement)
}
