package services

import (
	"time"

	"gitlab.com/fxpg/alphatrader/models"
)

type Ticker struct {
	Trading    models.Currency
	Settlement models.Currency
	Last       float64
}

//go:generate mockery -name=Exchange
type Exchange interface {
	GetTicker(trading models.Currency, settlement models.Currency) (*Ticker, error)
}

//go:generate mockery -name=UserRepository
type UserRepository interface {
	FindByStrId(userId string) (*models.User, error)
	Create(user *models.User) error
	UpdateToken(user *models.User) error
	UpdatePassword(user *models.User) error
}

//go:generate mockery -name=UserSettingRepository
type UserSettingRepository interface {
	FindById(settingId int) (*models.UserSetting, error)
	FindByUserId(userId int) ([]*models.UserSetting, error)
	Create(setting *models.UserSetting) error
	IsExist(userId int, exchangeId models.ExchangeID) (bool bool, err error)
	Delete(setting *models.UserSetting) (bool, error)
}

//go:generate mockery -name=ChartRepository
type ChartRepository interface {
	FindRange(exchangeId models.ExchangeID, duration int, trading string, settlement string, start time.Time, end time.Time) ([]models.Chart, error)
	FindN(exchangeId models.ExchangeID, duration int,
		trading string, settlement string, num int) ([]models.Chart, error)
	Find(exchangeId models.ExchangeID, duration int,
		trading string, settlement string) (models.Chart, error)
	Create(chart *models.Chart) error
	Truncate() error
}

//go:generate mockery -name=ExchangeRepository
type ExchangeRepository interface {
	FindAll() ([]models.ExchangeMap, error)
	FindCurrencyPairsByExchangeId(models.ExchangeID) ([]models.CurrencyPairMap, error)
}

//go:generate mockery -name=OrderRepository
type OrderRepository interface {
	Create(order *models.OrderData) error
	FindByTraderID(traderId int) ([]*models.OrderData, error)
}

//go:generate mockery -name=ResourceTokenRepository
type ResourceTokenRepository interface {
	Create(token *models.ResourceToken) error
	FindByUserId(userId int) (*models.ResourceToken, error)
	FindByKey(key string) (*models.ResourceToken, error)
	IsExist(userId int) (bool, error)
}

//go:generate mockery -name=TraderRepository
type TraderRepository interface {
	Create(trader *models.Trader) (traderId int, err error)
	FindById(traderId int) (*models.Trader, error)
	FindByUserId(userId int) ([]*models.Trader, error)
	UpdateStatus(traderId int, status string) (err error)
	Terminate(traderId int, reason string) (err error)
	FindByStatus(status string) ([]*models.Trader, error)
	FindRunningTrader(duration int) ([]*models.Trader, error)
	Delete(traderId int) (bool, error)
	ChangeStoppingToStopped() error
}

//go:generate mockery -name=GrpcServerRepository
type GrpcServerRepository interface {
	GetLoadBalancers() ([]string, error)
}
