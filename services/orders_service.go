package services

import (
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/models"
	"time"
)

type OrdersServiceImpl struct {
	OrderRepository OrderRepository `inject:""`
}

func (s *OrdersServiceImpl) Insert(userId int, traderId int, datetime time.Time, exchange string, orderId string, pair string, amount float64, price float64, order_type models.TradeType) error {
	exchangeId, err := models.ExchangeIDString(exchange)
	if err != nil {
		return errors.New("exchange id is not valid")
	}

	orderData := models.OrderData{
		ExchangeID: exchangeId,
		TraderID:   traderId,
		Pair:       pair,
		OrderID:    orderId,
		Datetime:   datetime,
		Status:     true,
		Price:      price,
		Amount:     amount,
		TradeType:  order_type,
	}

	if err := s.OrderRepository.Create(&orderData); err != nil {
		return errors.Wrap(err, "failed to create order")
	}
	return nil
}

func (s *OrdersServiceImpl) FindByTraderId(traderId int) ([]*models.OrderData, error) {
	orders, err := s.OrderRepository.FindByTraderID(traderId)
	if err != nil {
		return orders, errors.Wrap(err, "failed to get orders")
	}
	return orders, nil
}
