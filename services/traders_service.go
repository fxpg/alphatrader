package services

import (
	"context"
	"math/rand"
	"time"

	"fmt"

	"github.com/pkg/errors"
	pbm "gitlab.com/fxpg/alphatrader/atgrpc/master_server"
	"gitlab.com/fxpg/alphatrader/cmd/front-server/controllers/types"
	"gitlab.com/fxpg/alphatrader/logger"
	"gitlab.com/fxpg/alphatrader/models"
	"google.golang.org/grpc"
)

type TradersServiceImpl struct {
	UserRepository          UserRepository          `inject:""`
	ChartRepository         ChartRepository         `inject:""`
	TraderRepository        TraderRepository        `inject:""`
	GrpcServerRepository    GrpcServerRepository    `inject:""`
	ResourceTokenRepository ResourceTokenRepository `inject:""`
}

func (s *TradersServiceImpl) GetTrader(traderId int) (*models.Trader, error) {
	return s.TraderRepository.FindById(traderId)
}
func (s *TradersServiceImpl) GetTradersByDuration(duration int) ([]*models.Trader, error) {
	return s.TraderRepository.FindRunningTrader(duration)
}
func (s *TradersServiceImpl) ChangeStoppingToStopped() error {
	return s.TraderRepository.ChangeStoppingToStopped()
}

func (s *TradersServiceImpl) Set(setting *types.TraderSetting) (int, error) {
	traderId, err := s.TraderRepository.Create(&models.Trader{
		Description: setting.Description,
		UserID:      setting.UserId,
		TickSec:     setting.TickSec,
		Code:        setting.Code,
		Status:      "running",
	})
	if err != nil {
		return -1, err
	}
	return traderId, nil
}

func (s *TradersServiceImpl) Delete(traderId int) error {
	isDeleted, err := s.TraderRepository.Delete(traderId)
	if err != nil {
		return err
	}
	if !isDeleted {
		return errors.Errorf("there is no record of id %s", traderId)
	}
	return nil
}

func (s *TradersServiceImpl) Run(traderId int) error {
	addresses, err := s.GrpcServerRepository.GetLoadBalancers()
	if err != nil {
		fmt.Println(err)
		logger.Get().Info(err)
		return err
	}
	if len(addresses) == 0 {
		return errors.New("there is no LB")
	}
	atmaster := addresses[0]
	if len(addresses) > 2 {
		rand.Seed(time.Now().UnixNano())
		atmaster = addresses[rand.Intn(len(addresses)-1)]
	}
	logger.Get().Info(atmaster)
	trader, err := s.TraderRepository.FindById(traderId)
	if err != nil {
		return err
	}

	conn, err := grpc.Dial(atmaster+":50061", grpc.WithInsecure())
	if err != nil {
		return err
	}
	defer conn.Close()
	token, err := s.ResourceTokenRepository.FindByUserId(trader.UserID)
	if err != nil {
		return err
	}
	client := pbm.NewMasterServerClient(conn)
	res, err := client.RunTrader(context.Background(), &pbm.RunTraderRequest{
		UserId:  int64(trader.UserID),
		Code:    trader.Code,
		TickSec: int64(trader.TickSec),
		Token:   token.Key,
	})
	if err != nil {
		return err
	}
	if !res.IsRunned {
		return err
	}
	return nil
}

func (s *TradersServiceImpl) GetTraders(userId int) ([]*models.Trader, error) {
	traders, err := s.TraderRepository.FindByUserId(userId)
	if err != nil {
		return traders, err
	}
	return traders, nil
}

func (s *TradersServiceImpl) ChangeStatus(traderId int, status string) error {
	trader, err := s.TraderRepository.FindById(traderId)
	if err != nil {
		return err
	}
	if status == "running" && trader.Status == "terminated" && trader.TerminateReason == "compile failed" {
		return errors.New("compile failed trader cannot be runned")
	}
	return s.TraderRepository.UpdateStatus(traderId, status)
}
func (s *TradersServiceImpl) Terminate(traderId int, reason string) error {
	return s.TraderRepository.Terminate(traderId, reason)
}
