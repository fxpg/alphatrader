package services

import (
	"github.com/labstack/gommon/random"
	"gitlab.com/fxpg/alphatrader/models"
)

type GrpcServiceImpl struct {
	ResourceTokenRepository ResourceTokenRepository `inject:""`
}

func (s *GrpcServiceImpl) Auth(userId int, resourceToken string) (bool, error) {
	token, err := s.ResourceTokenRepository.FindByKey(resourceToken)
	if err != nil {
		return false, err
	}
	if token.UserID != userId {
		return false, nil
	}
	return true, nil
}

func (s *GrpcServiceImpl) GenerateResourceToken(userId int) (string, error) {
	t, err := s.ResourceTokenRepository.FindByUserId(userId)
	if err != nil {
		return "", err
	}
	if t == nil {
		key := random.New().String(30)
		token := &models.ResourceToken{
			UserID: userId,
			Key:    key,
		}
		err := s.ResourceTokenRepository.Create(token)
		if err != nil {
			return "", err
		}
		return token.Key, nil
	}
	return t.Key, nil
}
