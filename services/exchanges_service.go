package services

import (
	"gitlab.com/fxpg/alphatrader/models"
)

type ExchangesServiceImpl struct {
	ExchangeRepository ExchangeRepository `inject:""`
}

func (e *ExchangesServiceImpl) GetCurrencyPairsByExchangeID(exchangeId int) []models.CurrencyPairMap {
	currencyPairs, err := e.ExchangeRepository.FindCurrencyPairsByExchangeId(models.ExchangeID(exchangeId))
	if err != nil {
		return []models.CurrencyPairMap{}
	}
	return currencyPairs
}

func (e *ExchangesServiceImpl) GetExchanges() []models.ExchangeMap {
	exchangeMaps, err := e.ExchangeRepository.FindAll()
	if err != nil {
		return []models.ExchangeMap{}
	}
	return exchangeMaps
}
