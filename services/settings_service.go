package services

import (
	"github.com/pkg/errors"
	"gitlab.com/fxpg/alphatrader/models"
)

type SettingsServiceImpl struct {
	UserSettingRepository UserSettingRepository `inject:""`
}

func (s *SettingsServiceImpl) Create(userId int, exchangeId models.ExchangeID,
	apiKey string, secretKey string) error {
	setting := models.UserSetting{
		UserID:     userId,
		ExchangeID: exchangeId,
		APIKey:     apiKey,
		SecretKey:  secretKey,
	}

	err := s.UserSettingRepository.Create(&setting)
	return errors.Wrap(err, "failed to create user setting")
}

func (s *SettingsServiceImpl) FindAll(userId int) ([]*models.UserSetting, error) {
	return s.UserSettingRepository.FindByUserId(userId)
}

func (s *SettingsServiceImpl) IsExist(userId int, exchangeId models.ExchangeID) (bool bool, err error) {
	return s.UserSettingRepository.IsExist(userId, exchangeId)
}

func (s *SettingsServiceImpl) DeleteUserSetting(userId int, settingId int) (bool, error) {
	setting, err := s.UserSettingRepository.FindById(settingId)
	if err != nil {
		return false, err
	}
	if setting.UserID != userId {
		return false, errors.New("can not delete a setting owned by other user")
	}
	return s.UserSettingRepository.Delete(setting)
}
