package pprof

import (
	"gitlab.com/fxpg/alphatrader/logger"
	"net/http"
	_ "net/http/pprof"
)

type PProf struct {
}

func NewPProf() *PProf {
	return &PProf{}
}

func (p *PProf) Start() {
	go http.ListenAndServe("localhost:6060", nil)
	logger.Get().Info("pprof server is started at http://localhost:6060")
}
