package main

import (
	"fmt"

	"github.com/Jeffail/gabs"
)

func main() {

	var jsonStr = `[{"Name": "Ken", "Age": 32},{"Name": "Ken2", "Age": 32}]`

	jsonParsed, err := gabs.ParseJSON([]byte(jsonStr))
	if err != nil {
		panic(err)
	}
	fmt.Println()
	children, err := jsonParsed.Children()
	if err != nil {
		panic(err)
	}
	for _, child := range children {
		fmt.Println(child)
	}

}
