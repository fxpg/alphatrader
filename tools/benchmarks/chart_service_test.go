package benchmarks

import (
	"github.com/facebookgo/inject"
	"github.com/jinzhu/gorm"
	"gitlab.com/fxpg/alphatrader/models"
	"gitlab.com/fxpg/alphatrader/repositories"
	"gitlab.com/fxpg/alphatrader/services"
	"testing"
	"time"
)

// とりあえずベンチ−マーク取りたい用のスクリプト
// 保守性なし

func BenchmarkRange(b *testing.B) {
	dbConn := "fxpg:sHD9xDkHPdTf@tcp(localhost:3306)/alphatrader?charset=utf8&parseTime=True&loc=UTC"
	db, err := gorm.Open("mysql", dbConn)
	db.LogMode(true)
	if err != nil {
		b.Fatal(err)
	}

	var s services.ChartServiceImpl
	var g inject.Graph
	if err := g.Provide(
		&inject.Object{Value: db},
		&inject.Object{Value: &repositories.ChartRepositoryGorm{}},
		&inject.Object{Value: &s},
	); err != nil {
		b.Fatal(err)
	}
	if err := g.Populate(); err != nil {
		b.Fatal(err)
	}

	for i := 0; i < b.N; i++ {
		charts, err := s.Range(2, 60, models.Ethereum, models.Bitcoin,
			time.Date(2017, 10, 2, 1, 0, 0, 0, time.UTC),
			time.Date(2017, 10, 4, 3, 0, 0, 0, time.UTC))
		if err != nil {
			b.Fatal(err)
		}
		b.Logf("chart num: %d", len(charts))
	}
}
