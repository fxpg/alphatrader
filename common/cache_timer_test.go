package common

import (
	"testing"
	"time"
)

func TestCacheTimer(t *testing.T) {
	timer := NewCacheTimer(5 * time.Second)
	if !timer.IsExpired() {
		t.Fatal("timer is not expired")
	}

	timer.Update()
	if timer.IsExpired() {
		t.Fatal("timer is expired")
	}
}
