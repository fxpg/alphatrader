package common

import (
	"sync"
	"time"
)

var (
	unix_epoch_time = time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
)

// キャッシュの保持期間を管理する構造体
type CacheTimer struct {
	lastUpdated time.Time
	expires     time.Duration

	mtx *sync.Mutex
}

func NewCacheTimer(expires time.Duration) *CacheTimer {
	return &CacheTimer{
		lastUpdated: unix_epoch_time,
		expires:     expires,
		mtx:         new(sync.Mutex),
	}
}

func (t *CacheTimer) Update() {
	t.mtx.Lock()
	defer t.mtx.Unlock()

	t.lastUpdated = time.Now()
}

func (t *CacheTimer) IsExpired() bool {
	t.mtx.Lock()
	defer t.mtx.Unlock()

	return time.Now().Sub(t.lastUpdated) > t.expires
}

func (t *CacheTimer) Expire() {
	t.mtx.Lock()
	defer t.mtx.Unlock()

	t.lastUpdated = unix_epoch_time
}
