package common

type HashCode string

type Hashable interface {
	hashCode() HashCode
}
