package common

import (
	"github.com/jinzhu/gorm"
)

func DBConnect(DBConnection string) *gorm.DB {
	db, err := gorm.Open("mysql", DBConnection)
	if err != nil {
		return nil
	}
	return db
}
